<?php
class Calculator
{
    public $num = null;
    public $num2 = null;
    public function setData($a = '', $b = '')
    {
        $this-> num = $a;
        $this-> num2 =$b;
    }
    public function sumData()
    {
        return $this->num + $this->num2;
    }
    public function getData(){
     return $this->sumData();
    }
}

$obj = new Calculator();

$obj->setData(50, 20);
echo "Total result is: " . $obj->getData();

//How to know total method in this part.
echo "<br>";

$class_methods = get_class_methods(new Calculator());
foreach ($class_methods as $method_name) {
    echo "$method_name\n";
}


