<?php
class Dada
{
    public function __construct(){
        echo "1st Constructor.<br/>";
    }
}

class subDada extends Dada{
    function __construct()
    {
        parent::__construct();
        echo "1st Constructors Sub Constructor.";
    }
}

$obj = new SubDada();
