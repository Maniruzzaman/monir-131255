<?php
include_once("../../../../../../vendor/autoload.php");

use App\Module_1\University\Subjects\Batch\Students\Students
    ;
$obj = new Students();

$value = $obj->setData($_GET)->show();

?>


<html>
<head>
    <title>List of Students</title>
</head>
<body>
<a href="index.php">Back to List</a>
<table border="1px">
    <tr>
        <td>Serial</td>
        <td>Title</td>
        <td>Action</td>
    </tr>

        <tr>
            <td><?php echo $value['id'] ?></td>
            <td><?php echo $value['title'] ?></td>
            <td>
                <a href="show.php?id=<?php echo $value['id'] ?>">Edit</a>
            </td>
        </tr>

</table>
</body>
</html>

