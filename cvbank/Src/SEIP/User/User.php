<?php


class User
{
    public $username = '';
    public $email = '';
    public $pdo = '';
    public $password = '';

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');

    }

    public function setData($data = '')
    {
        if (array_key_exists('username', $data)) {
            $this->username = $data['username'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('password', $data)) {
            $this->password = $data['password'];
        }
        return $this;

    }

    public function store()
    {
        try {
            $query = "INSERT INTO `users` (`id`, `username`, `email`, `password`, `token`) VALUES (:id, :uname, :email, :pw, :tk)";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => null,
                    ':uname' => $this->username,
                    ':email' => $this->email,
                    ':pw' => $this->password,
                    ':tk' => 'dsfsdfsdfsf',
                )
            );
            if ($stmt) {
                $_SESSION['message'] = "Successfully Registered";
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function userAvailability()
    {
        try {
            $query = "SELECT  * FROM `users` WHERE username = '$this->username' OR email = '$this->email'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function login()
    {
        try {
            $query = "SELECT  * FROM `users` WHERE ( username = '$this->username' OR email = '$this->username') AND password = '$this->password'";

            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if (!empty($data)) {
                $_SESSION['user_info'] = $data;
                header('location:admin/index.php');
            } else {
                $_SESSION['fail'] = "Invalid Username, Email Or Password";
                header('location:index.php');
            }

            return $data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }

    public function index($nooftems = '', $offset)
    {
        try {
            $query = "SELECT SQL_CALC_FOUND_ROWS * FROM `students` WHERE deleted_at='0000-00-00 00:00:00' LIMIT $nooftems OFFSET $offset";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            $subquery = 'SELECT FOUND_ROWS()';
            $row = $this->pdo->query($subquery)->fetch(PDO::FETCH_COLUMN);
            $data['totalRow'] = $row;
            return $data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

//    public function getNoOfRows()
//    {
//        $query = "SELECT * FROM `students` WHERE deleted_at='0000-00-00 00:00:00'";
//        $stmt = $this->pdo->prepare($query);
//        $stmt->execute();
//        $data = count($stmt->fetchAll());
//        return $data;
//    }


    public function show()
    {
        try {
            $query = "SELECT * FROM `students` WHERE unique_id='$this->id'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if (empty($data)) {
                $_SESSION['message'] = "Opps something going wrong";
                header('location:index.php');
            } else {
                return $data;
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function update()
    {
        try {
            $query = "UPDATE students SET title=:title where id=:id";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id,
                    ':title' => $this->name
                )
            );
            if ($stmt) {
                $_SESSION['message'] = "Successfully Updated";
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function trash()
    {
        try {
            $query = "UPDATE students SET deleted_at=:datetme where id=:id";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id,
                    ':datetme' => date('Y-m-d h:m:s'),
                )
            );
            if ($stmt) {
                $_SESSION['message'] = "Successfully Deleted";
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function trashlist()
    {

        try {
            $query = "SELECT * FROM `students` WHERE deleted_at!='0000-00-00 00:00:00'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function restore()
    {
        try {
            $query = "UPDATE students SET deleted_at=:datetme where id=:id";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id,
                    ':datetme' => '0000-00-00 00:00:00',
                )
            );
            if ($stmt) {
                $_SESSION['message'] = "Successfully Restored !!!";
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function delete()
    {
        try {
            $query = "DELETE FROM `students` WHERE `students`.`id` =$this->id";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            if ($stmt) {
                $_SESSION['message'] = "<h2>Successfully Deleted</h2>";
                header('location:trashlist.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function search($noOfItems = '', $offset = '')
    {
        $key = "'%" . $this->keyword . "%'";
        try {
            $query = "SELECT SQL_CALC_FOUND_ROWS * FROM `students` WHERE title LIKE $key LIMIT $noOfItems OFFSET $offset";
//            echo $query;
//            die();
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            $subquery = 'SELECT FOUND_ROWS()';
            $row = $this->pdo->query($subquery)->fetch(PDO::FETCH_COLUMN);
            $data['totalRow'] = $row;
            return $data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}

