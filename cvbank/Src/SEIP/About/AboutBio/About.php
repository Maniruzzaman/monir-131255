<?php

class About
{
    public $name = '';
    public $phone = '';
    public $bio = '';
    public $id = '';

    public function setData($data = '')
    {
        if(array_key_exists('title', $data)){
            $this->name = $data['title'];
        }
        if(array_key_exists('phone', $data)){
            $this->phone = $data['phone'];
        }
        if(array_key_exists('bio', $data)){
            $this->bio = $data['bio'];
        }
        if(array_key_exists('id', $data)){
            $this->id = $data['id'];
        }

        return $this;
    }
    public function store()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "INSERT INTO `abouts` (`id`, `title`, `phone`, `bio`) VALUES (:a, :b, :c, :d)";

            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' => null,
                ':b' => $this->name,
                ':c' => $this->phone,
                ':d' => $this->bio
            ));
            if($stmt){
                header('location:create.php');
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function index()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');

            $query = "SELECT * FROM `abouts`";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function show()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM `abouts` WHERE id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            return $data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function update()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');

            $query = "UPDATE `abouts` SET `title`=:title,`phone`=:phone,`bio`=:bio WHERE id=:id";

         //   $query = "UPDATE abouts SET title=:title WHERE id=:id";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
               /* ':id' => $this->id,
                ':title' => $this->name*/

                ':id' => $this->id,
                ':title' => $this->name,
                ':phone' => $this->phone,
                ':bio' => $this->bio
            ));
            if ($stmt) {
                session_start();
                $_SESSION['message'] = "Successfully Updated";
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }


    public function delete()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "DELETE FROM `abouts` WHERE `abouts`.`id` = $this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            if($stmt){
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}
