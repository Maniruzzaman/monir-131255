<?php
include_once("../../Src/SEIP/User/User.php");
$obj = new User();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($_POST['password'] == $_POST['confirm_password']) {
        if (strlen($_POST['password']) > 2) {
            $user = $obj->setData($_POST)->userAvailability();
            if (empty($user)) {
                $obj->setData($_POST)->store();
            } else {
                $_SESSION['fail'] = "Sorry! Username or email already exists!";
                header('location:index.php#toregister');
            }

        } else {
            $_SESSION['fail'] = "You have to provide at least 3 character!";
            header('location:index.php#toregister');
        }

    } else {
        $_SESSION['fail'] = "Password not matched !";
        header('location:index.php#toregister');
    }
}
