<?php
include_once("../../../../Src/SEIP/About/AboutBio/About.php");

$obj = new About();
$allData = $obj->index();
?>

<html>
<head>
    <title>Student Information List</title>
</head>
<body>

<table border="0px" align="center" width="700px">
    <tr>
        <td>Id</td>
        <td>Name</td>
        <td>Phone</td>
        <td>Bio</td>
        <td>Action</td>
    </tr>
    <?php
    $serial = 1;
    foreach ($allData as $key => $value){

    ?>
    <tr>
        <td><?php echo $serial++; ?></td>
        <td><?php echo $value['title'] ?></td>
        <td><?php echo $value['phone'] ?></td>
        <td><?php echo $value['bio'] ?></td>
        <td>
            <a href="show.php?id=<?php echo $value["id"]; ?>">Views Details</a> |
            <a href="edit.php?id=<?php echo $value["id"]; ?>">Edit</a> |
            <a href="delete.php?id=<?php echo $value["id"]; ?>">Delete</a>
        </td>
    </tr>
    <?php } ?>
</table>
