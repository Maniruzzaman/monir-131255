<?php
include_once("../../../../Src/SEIP/About/AboutBio/About.php");

$obj = new About();
$obj->setData($_GET);
$value = $obj->show();
?>

<html>
<head>
    <title>Student Information List</title>
</head>
<body>

<table border="0px" align="center" width="700px">
    <tr>
        <td>Id</td>
        <td>Name</td>
        <td>Phone</td>
        <td>Bio</td>
        <td>Action</td>
    </tr>

        <tr>
            <td><?php echo $value['id']; ?></td>
            <td><?php echo $value['title'] ?></td>
            <td><?php echo $value['phone'] ?></td>
            <td><?php echo $value['bio'] ?></td>
            <td>
                <a href="edit.php?id=<?php echo $value["id"]; ?>">Edit</a> |
                <a href="delete.php?id=<?php echo $value["id"]; ?>">Delete</a>
            </td>
        </tr>
</table>
