<?php
include_once "../../../vendor/autoload.php";
use App\SEIP\User\User;
$obj = new User();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $obj->setData($_POST)->login();
}