<?php
/*echo "<pre>";
print_r($_POST);
die();*/
session_start();
if (empty($_SESSION['user_info'])) {
    $_SESSION['fail'] = "Sory! Your are not authorized to access this page";
    header('location:../index.php');
}
include_once "../../../../vendor/autoload.php";
use App\SEIP\Resume\Experience\Experience;
$obj = new Experience();


if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if(!empty($_POST['designation_name'])){
        if(!empty($_POST['company_name'])){
            if(!empty($_POST['starting_date'])){
                if(!empty($_POST['ending_date'])){
                    if(!empty($_POST['company_location'])){
                        $obj->setData($_POST)->store($_SESSION['user_info']['id']);
                    }else{
                        session_start();
                        $_SESSION['fail'] = "Company location can't be empty.";
                        header('location: create.php');
                    }
                }else{
                    session_start();
                    $_SESSION['fail'] = "Ending date can't be empty.";
                    header('location: create.php');
                }
            }else{
                session_start();
                $_SESSION['fail'] = "Starting date can't be empty.";
                header('location: create.php');
            }
        }else{
            session_start();
            $_SESSION['fail'] = "Company location can't be empty.";
            header('location: create.php');
        }
    }else{
        session_start();
        $_SESSION['fail'] = "Designation title can't be empty.";
        header('location: create.php');
    }
}


