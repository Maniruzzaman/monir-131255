<?php
/*echo "<pre>";
print_r($_POST);
print_r($_FILES);
die();*/
session_start();
if (empty($_SESSION['user_info'])) {
    $_SESSION['fail'] = "Sory! Your are not authorized to access this page";
    header('location:../index.php');
}
include_once "../../../../vendor/autoload.php";
use  App\SEIP\About\Fact\Fact;
$obj = new Fact();


if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if(!empty($_POST['fact_name'])){
        if(!empty($_POST['fact_count'])){            
            if(!empty($_FILES['fact_image']['name'])){

                $image_name = $_FILES['fact_image']['name'];
                $image_type = $_FILES['fact_image']['type'];
                $image_tmp_location  = $_FILES['fact_image']['tmp_name'];
                $image_size = $_FILES['fact_image']['size'];
                $my_image_extension = strtolower(end(explode(".",$image_name)));
                $required_formate = array('jpg','jpeg','png','gif');

                if(in_array($my_image_extension, $required_formate)){
                    
                    $_POST['fact_image'] = $image_name;
                    $_POST['fact_img_location'] = $image_tmp_location;
                    $_POST['fact_name'] = filter_var($_POST['fact_name'],FILTER_SANITIZE_STRING);
                    $obj->setData($_POST)->updateWithImage();

                }else{
                    session_start();
                    $_SESSION['fail'] = "Image formate should be jpg, png, gif,jpeg.";
                    header('location: create.php');
                }
            }else{
                $_POST['fact_name'] = filter_var($_POST['fact_name'],FILTER_SANITIZE_STRING);
                $obj->setData($_POST)->updateWithoutImage();
            }

        }else{
            session_start();
            $_SESSION['fail'] = "Fact counter can't be empty.";
            header('location: create.php');
        }
    }else{
        session_start();
        $_SESSION['fail'] = "Fact title can't be empty.";
        header('location: create.php');
    }
}


