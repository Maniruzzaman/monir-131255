<?php

session_start();
if (empty($_SESSION['user_info'])) {
    $_SESSION['fail'] = "Sory! Your are not authorized to access this page";
    header('location:../index.php');
}
include_once "../../../vendor/autoload.php";
use App\SEIP\Porfolio\Porfolio;
$obj = new Porfolio(); 


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if(!empty($_POST['portfolio_name'])){
		if(!empty($_POST['portfolio_description'])){
			if(!empty($_POST['portfolio_category'])){

if(!empty($_FILES['portfolio_image']['name'])){


					$image_name = $_FILES['portfolio_image']['name'];
					$image_type = $_FILES['portfolio_image']['type'];
					$image_tmp_location  = $_FILES['portfolio_image']['tmp_name'];
					$image_size = $_FILES['portfolio_image']['size'];
					$my_image_extension = strtolower(end(explode(".",$image_name)));
					$required_formate = array('jpg','jpeg','png','gif');

		    		if(in_array($my_image_extension, $required_formate)){
						if($image_size>20000){

							$_POST['portfolio_image'] = $image_name;
							$_POST['portfolio_img_location'] = $image_tmp_location;

							$_POST['portfolio_name'] = filter_var($_POST['portfolio_name'],FILTER_SANITIZE_STRING);

							$_POST['portfolio_description'] = filter_var($_POST['portfolio_description'],FILTER_SANITIZE_STRING);
							$obj->setData($_POST)->updateWithImage();
							
						}else{
							session_start();
						    $_SESSION['fail'] = "Image size should be 2mb .";
						    header('location: create.php');
						}
					}else{
						session_start();
					    $_SESSION['fail'] = "Image formate should be jpg, png, gif,jpeg.";
					    header('location: create.php');
					}


				}else{
					$_POST['portfolio_name'] = filter_var($_POST['portfolio_name'],FILTER_SANITIZE_STRING);
					$_POST['portfolio_description'] = filter_var($_POST['portfolio_description'],FILTER_SANITIZE_STRING);
					$obj->setData($_POST)->updateWithoutImage();
				}			
			}else{
				session_start();
				$_SESSION['fail'] = "Portfolio category can't be empty.";
				header('location: create.php');
			}
		}else{
			session_start();
			$_SESSION['fail'] = "Portfolio description can't be empty.";
			header('location: create.php');
		}
	}else{
		session_start();
		$_SESSION['fail'] = "Portfolio name can't be empty.";
		header('location: create.php');
	}
}io;
$obj = new Porfolio(); 