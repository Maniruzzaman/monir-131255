<?php

session_start();
if (empty($_SESSION['user_info'])) {
    $_SESSION['fail'] = "Sory! Your are not authorized to access this page";
    header('location:../index.php');
}
include_once "../../../vendor/autoload.php";
use App\SEIP\Skill\Skill;
$obj = new Skill();

if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{
    if(!empty($_POST['skill_title']))
    {
        if (!empty($_POST['skill_description'])) 
        {
            if(!empty($_POST['Skill_level']))
            {
                if(!empty($_POST['Skill_experience']))
                {
                     if(!empty($_POST['skill_area']))
                     {
                        $_POST['skill_title'] = filter_var($_POST['skill_title'],FILTER_SANITIZE_STRING);
                        $_POST['skill_description'] = filter_var($_POST['skill_description'],FILTER_SANITIZE_STRING);
                        $obj->setData($_POST)->update();
                    }else{
                        session_start();
                        $_SESSION['fail'] = "Skill area can't be empty.";
                        header('location: create.php');
                    }
                }else{
                    session_start();
                    $_SESSION['fail'] = "Skill experance can't be empty.";
                    header('location: create.php');
                }
            }else{
                session_start();
                $_SESSION['fail'] = "Skill experience can't be empty.";
                header('location: create.php');
            }
        }else{
            session_start();
            $_SESSION['fail'] = "Skill description can't be empty.";
            header('location: create.php');
        }
    }else{
        session_start();
        $_SESSION['fail'] = "Skill title can't be empty.";
        header('location: create.php');
    }
}