<?php
session_start();
if (empty($_SESSION['user_info'])) {
    $_SESSION['fail'] = "Sory! Your are not authorized to access this page";
    header('location:../index.php');
}
?>
<?php
include "../../../vendor/autoload.php";
use App\SEIP\Skill\Skill;
$obj = new Skill();
$data = $obj->setData($_GET)->show();
use App\SEIP\Setting\Setting;
$obj = new Setting();
$showSettingData = $obj->ShowImageForProfile($_SESSION['user_info']['id']);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CvBank allkinds of cv here</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="../../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../../../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
    <link href="../../../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
    <link href="../../../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../../assets/js/plugins/visualization/d3/d3.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/pickers/daterangepicker.js"></script>

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../../ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/forms/styling/uniform.min.js"></script>

    <script type="text/javascript" src="../../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="../../../assets/js/pages/editor_ckeditor.js"></script>
    <script type="text/javascript" src="../../../assets/js/pages/form_layouts.js"></script>
    <script type="text/javascript" src="../../../assets/js/pages/dashboard.js"></script>
    <!-- /theme JS files -->

</head>

<body>
<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="index.php"><img src="../../../assets/images/CoderGang.png" alt="" width="200px" height="35px"></a>
        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                <?php if ($showSettingData['featured_img']) {  ?>
                    <img src='<?php echo "../../../images/setting/". $showSettingData['featured_img']; ?> ' alt="">
                <?php } else {?>
                <img src="../../../images/setting/profileDefaultImage.jpg" alt="" >
                <?php } ?>
                    <span><?php echo $_SESSION['user_info']['username'] ?></span>
                    <i class="caret"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a target="_blank" href="../Home/index.php"><i class="icon-user-plus"></i> My profile</a></li>
                    <li class="divider"></li>
                    <li><a href="../Setting/create.php"><i class="icon-cog5"></i> Account settings</a></li>
                    <li><a href="../Admin/logout.php"><i class="icon-switch2"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->

<!-- Page container -->
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">
                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul style="padding: 0px;" class="navigation navigation-main navigation-accordion">

                            <li class="active">
                                <a href="../Admin/index.php"><i class="icon-home4"></i><span>Dashboard</span></a>
                            </li>

                            <li>
                                <a href="#"><i class="icon-cog5"></i> <span>Settings</span></a>
                                 <ul>
                                    <li><a href="../Setting/create.php">Setting</a></li>
                                    <?php if ($_SESSION['user_info']['user_role'] == 2) { ?>
                                    <li><a href="../Setting/show.php?id=<?php echo $_SESSION['user_info']['id']?>">My Setting</a></li>
                                    <li><a href="../Setting/index.php">View Setting</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><i class="icon-insert-template"></i> <span>About</span></a>
                                <ul>
                                    <li><a href="#">About Bio</a>
                                        <ul>
                                            <li><a href="../About/AboutBio/create.php">Add Bio</a></li>
                                            <li><a href="../About/AboutBio/show.php?id=<?php echo $_SESSION['user_info']['id']?>">My Bio</a></li>
                                            <?php if ($_SESSION['user_info']['user_role'] == 2) { ?>
                                                <li><a href="../About/AboutBio/index.php">View Bio</a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <li><a href="#">About Hobby</a>
                                        <ul>
                                            <li><a href="../About/Hobbie/create.php">Add hobby</a></li>
                                            <li><a href="../About/Hobbie/show.php?id=<?php echo $_SESSION['user_info']['id']?>">My Hobby</a></li>
                                            <?php if ($_SESSION['user_info']['user_role'] == 2) { ?>
                                                <li><a href="../About/Hobbie/index.php">View Hobby</a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <li><a href="#">About fact</a>
                                        <ul>
                                            <li><a href="../About/Fact/create.php">Add fact</a></li>
                                            <li><a href="../About/Fact/show.php?id=<?php echo $_SESSION['user_info']['id']?>">My fact</a></li>
                                            <?php if ($_SESSION['user_info']['user_role'] == 2) { ?>
                                                <li><a href="../About/Fact/index.php">View fact</a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><i class="icon-stack2"></i> <span>Resume</span></a>
                                <ul>
                                    <li><a href="#">Add Education</a>
                                        <ul>
                                            <li><a href="../Resume/Education/create.php">Add education</a></li>
                                            <li><a href="../Resume/Education/show.php?id=<?php echo $_SESSION['user_info']['id']?>">My education</a></li>
                                            <?php if ($_SESSION['user_info']['user_role'] == 2) { ?>
                                                <li><a href="../Resume/Education/index.php">View education</a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <li><a href="#">Add Experience</a>
                                        <ul>
                                            <li><a href="../Resume/Experience/create.php">Add Experience</a></li>
                                            <li><a href="../Resume/Experience/show.php?id=<?php echo $_SESSION['user_info']['id']?>">My Experience</a></li>
                                            <?php if ($_SESSION['user_info']['user_role'] == 2) { ?>
                                                <li><a href="../Resume/Experience/index.php">View Experience</a></li>
                                            <?php } ?>
                                        </ul>    
                                    </li>
                                    <li><a href="#">Add Awards</a>
                                        <ul>
                                            <li><a href="../Resume/Award/create.php">Add Awards</a></li>
                                            <li><a href="../Resume/Award/show.php?id=<?php echo $_SESSION['user_info']['id']?>">My Awards</a></li>
                                            <?php if ($_SESSION['user_info']['user_role'] == 2) { ?>
                                                <li><a href="../Resume/Award/index.php">View Awards</a></li>
                                            <?php } ?>
                                        </ul> 
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><i class="icon-copy"></i> <span>Publications</span></a>
                                <ul>
                                    <li><a href="../post/create.php">Add post</a></li>
                                    <li><a href="../post/show.php?id=<?php echo $_SESSION['user_info']['id']?>">My post</a></li>
                                    <?php if ($_SESSION['user_info']['user_role'] == 2) { ?>
                                        <li><a href="../post/index.php">View post</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><i class="icon-task"></i> <span>Services</span></a>
                                <ul>
                                    <li><a href="../Service/create.php">Add service</a></li>
                                    <li><a href="../Service/show.php?id=<?php echo $_SESSION['user_info']['id']?>">My service</a></li>
                                    <?php if ($_SESSION['user_info']['user_role'] == 2) { ?>
                                        <li><a href="../Service/index.php">View service</a></li>
                                    <?php } ?>
                                </ul> 
                            </li>
                            <li>
                                <a href="#"><i class="icon-gift"></i> <span>Skills</span></a>
                                <ul>
                                    <li><a href="../Skill/create.php">Add skills</a></li>
                                    <li><a href="../Skill/show.php?id=<?php echo $_SESSION['user_info']['id']?>">My skills</a></li>
                                    <?php if ($_SESSION['user_info']['user_role'] == 2) { ?>
                                        <li><a href="../Skill/index.php">View skills</a></li>
                                    <?php } ?>
                                </ul> 
                            </li>
                            <li>
                                <a href="#"><i class="icon-stack"></i> <span>Portfolio</span></a>
                                <ul>
                                    <li><a href="../Portfolio/create.php">Add Portfolio</a></li>
                                    <li><a href="../Portfolio/show.php?id=<?php echo $_SESSION['user_info']['id']?>">My Portfolio</a></li>
                                    <?php if ($_SESSION['user_info']['user_role'] == 2) { ?>
                                    <li><a href="../Portfolio/index.php">View Portfolio</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <li>
                                <a href="../Contact/create.php"><i class="icon-user-plus"></i> <span>Contact</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->
            </div>
        </div>
        <!-- /main sidebar -->

        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Dashboard</h4>
                    </div>
                </div>

                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        <li><a href="../Admin/index.php"><i class="icon-home2 position-left"></i> Home</a></li>
                        <li class="active">Skill Edit</li>
                    </ul>
                </div>
            </div>
            <!-- /page header -->
            <!-- Content area -->
            <div class="content">
                <!-- Main charts -->
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Basic layout-->
                        <?php
                            if(isset($_SESSION['message'])){
                            echo "<h1 style='color:green;font-weight:bold;'>".$_SESSION['message']."</h1>";
                            unset($_SESSION['message']);
                            }
                        ?>
                        <?php
                            if(isset($_SESSION['fail'])){
                            echo "<h1 style='color:red;font-weight:bold;'>".$_SESSION['fail']."</h1>";
                            unset($_SESSION['fail']);
                            }
                        ?>
                        <form action="update.php" method="POST" class="form-horizontal">
                        <input type="hidden" name="id" value="<?php echo $data['id']; ?>" />
                        <input type="hidden" name="user_id" value="<?php echo $data['user_id']; ?>" />
                            <div class="panel panel-flat">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Skill title</label>
                                    <div class="col-lg-9">
                                    <input class="form-control" type="text" name="skill_title" value="<?php echo $data['title']; ?>" placeholder="Enter your skill name">
                                    </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Skill Description</label>
                                        <div class="col-lg-9">
                                            <textarea name="skill_description" cols="30" rows="10"><?php echo $data['description']; ?></textarea>
                                            <script type="text/javascript">
                                                if ( typeof CKEDITOR == 'undefined' )
                                                {
                                                    document.write(
                                                        '<strong><span style="color: #ff0000">Error</span>: CKEditor not found</strong>.' +
                                                        'This sample assumes that CKEditor (not included with CKFinder) is installed in' +
                                                        'the "/ckeditor/" path. If you have it installed in a different place, just edit' +
                                                        'this file, changing the wrong paths in the &lt;head&gt; (line 5) and the "BasePath"' +
                                                        'value (line 32).' ) ;
                                                }
                                                else
                                                {
                                                    var editor = CKEDITOR.replace( 'skill_description' );
                                                    //editor.setData( '<p>Just click the <b>Image</b> or <b>Link</b> button, and then <b>&quot;Browse Server&quot;</b>.</p>' );
                                                }

                                            </script>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Skill level</label>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <select name="Skill_level" class="form-control" data-width="100%">

                                                <option>Select skill level</option>
                                                <option value="Intermediate" <?php if($data['level'] == 'Intermediate'): echo 'selected="selected"'; endif; ?> >Intermediate</option>
                                                <option value="Professional" <?php if($data['level'] == 'Professional'): echo 'selected="selected"'; endif; ?> >Professional</option>
                                                <option value="Advance" <?php if($data['level'] == 'Advance'): echo 'selected="selected"'; endif; ?> >Advance</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Skill Experience</label>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <select name="Skill_experience" class="form-control" data-width="100%">
                                                    <option>Select skill experience year</option>
                                                    <option value="1" <?php if($data['experience'] == 1): echo 'selected="selected"'; endif; ?> >1 year</option>
                                                    <option value="2" <?php if($data['experience'] == 2): echo 'selected="selected"'; endif; ?> >2 years</option>
                                                    <option value="3" <?php if($data['experience'] == 3): echo 'selected="selected"'; endif; ?> >3 years</option>
                                                    <option value="4" <?php if($data['experience'] == 4): echo 'selected="selected"'; endif; ?> >4 years</option>
                                                    <option value="5" <?php if($data['experience'] == 5): echo 'selected="selected"'; endif; ?> >5 years</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Skill area</label>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <?php
                                                    $checkboxData = unserialize($data['experience_area']);
                                                ?>                                    
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="skill_area[]" value="PHP" <?php if(in_array('PHP', $checkboxData)): echo ' checked="checked"'; endif; ?>/>PHP
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="skill_area[]" value="JAVA" <?php if(in_array('JAVA', $checkboxData)): echo ' checked="checked"'; endif; ?> />JAVA
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="skill_area[]" value="HTML" <?php if(in_array('HTML', $checkboxData)): echo ' checked="checked"'; endif; ?>/>HTML
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="skill_area[]" value="CSS" <?php if(in_array('CSS', $checkboxData)): echo ' checked="checked"'; endif; ?>/>CSS
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary">Update form <i class="icon-arrow-right14 position-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- /basic layout -->
                    </div>
                </div>
                <!-- /main charts -->

                <!-- Footer -->
                <div class="footer text-muted">&copy; 2017. <a href="#">CvBank</a> by <a href="#" target="_blank">CodeGand</a>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->
</div>
<!-- /page container -->
</body>
</html>
