<?php
include_once "vendor/autoload.php";
use App\SEIP\Home\Home;

$obj = new Home();
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (isset($_GET['search']) && !empty($_GET['search'])) {
        $searchAllItems = $obj->setData($_GET)->search();
    } else {
        echo "<h1 style='color:green;font-weight:bold;'>Please enter something and press serach button</h1>";
    }
}


if(isset($_SESSION['fail'])){
    session_start();
    echo "<h1 style='color:red;font-weight:bold;'>".$_SESSION['fail']."</h1>";
    unset($_SESSION['fail']);
    }
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<div class="total_body" style="height: 109px;width: 100%;">
    <div class="login_logout" style="float:right; margin-top:10px;">
        <a href="views/SEIP/User/index.php#tologin"
           style="font-size: 20px;background: #4285F4;color: #fff;padding: 10px 15px;text-decoration: none;">Sign In</a>
        <a href="views/SEIP/User/index.php#toregister"
           style="font-size: 20px;background: #4285F4;color: #fff;padding: 10px 15px;text-decoration: none; margin-left:10px;">Create
            Account</a>
    </div>
    <div class="search" style="width:600px;">
        <?php
        if (isset($_SESSION['message'])) {
            echo "<h1 style='color:red;font-weight:bold;'>" . $_SESSION['message'] . "</h1>";
            unset($_SESSION['message']);
        }
        ?>
        <form action="index.php" method="get">
            <table>
                <tr>
<input type="text" name="search" id="" style="width: 500px;padding: 10px;border: 2px solid #4285F4;
    border-radius: 5px;" autofocus/></br>
<input type="submit" value="Search" style="width: 100px;padding: 10px;margin-top:10px;margin-left:202px;border:0px;"/>
                </tr>
            </table>
        </form>
    </div>

    <?php
    if (isset($_GET['search'])) {

        if (!empty($searchAllItems)) {
            echo "Search Result  For <b>" . $_GET['search'] . "</b>";
            foreach ($searchAllItems as $value) {
                ?>
                <div style="margin-top:20px;height: 120px;">
                    <table>
<?php if($value['featured_img']) { ?> 
<tr>
    <img style="float:left;" width="100"
         src='<?php echo "images/setting/" . $value['featured_img']; ?> ' alt="image"/>
</tr>

<?php } else { ?> 
<tr>
<img style="float:left;" width="100" src='images/setting/profileDefaultImage.jpg' alt="image" />
</tr>

<?php } ?> 
<tr>
                            <td><b>SearchItem</b></td>
                            <td>
<a target="_blank" href="Views/SEIP/Home/index2.php?search=<?php echo $value['username']; ?>"><?php echo $value['username']; ?></a>
                            </td></tr>
                        </tr>
                        <tr>
                            <td><b>User name</b></td>
                            <td><?php echo $value['title']; ?></td>
                        </tr>
                        <tr>
                            <td><b>User description</b></td>
                            <td><?php echo $value['description']; ?></td>
                        </tr>
                        </tr>
                    </table>
                </div>
                <?php
            }
        } else {
            /*session_start();
            $_SESSION['message']="Not found.";
            header('location:index.php');*/
            echo "Data not found For <b>" . $_GET['search'] . "</b>";
        }
    }
    
    ?>

</div>
</body>
</html>
