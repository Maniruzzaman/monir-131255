<?php
namespace App\SEIP\Porfolio;
use PDO;
Class Porfolio{
	public $id='';
	public $user_id='';
	public $previousDbImg='';
	public $portfolioName='';
	public $portfolioDescription='';
	public $portfolioImageLocation = '';
	public $portfolioImage='';
	public $portfolioCategory='';

	public function setData($data=''){
		if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }        
		if(array_key_exists('user_id',$data)){
            $this->user_id = $data['user_id'];
        }      
		if(array_key_exists('previous_db_img',$data)){
            $this->previousDbImg = $data['previous_db_img'];
        }
		if(array_key_exists('portfolio_name', $data)){
			$this->portfolioName = $data['portfolio_name'];
		}
		if(array_key_exists('portfolio_description', $data)){
			$this->portfolioDescription = $data['portfolio_description'];
		}
		if(array_key_exists('portfolio_img_location', $data)){
			$this->portfolioImageLocation = $data['portfolio_img_location'];
		}	
		if(array_key_exists('portfolio_image', $data)){
			$this->portfolioImage = uniqid().time().$data['portfolio_image'];
		}	
		if(array_key_exists('portfolio_category', $data)){
			$this->portfolioCategory = $data['portfolio_category'];
		}			
		return $this;
	}

	public function store($userId){
		 try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
$query = "INSERT INTO `portfolios` (`id`,`user_id`,`title`,`description`,`img`,`category`,`created_at`,`updated_at`,`deleted_at`) VALUES(:a,:b,:c,:d,:e,:f,:g,:h,:i)";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' =>null,
                ':b' => $userId,
                ':c' => $this->portfolioName,
                ':d' => $this->portfolioDescription,
                ':e' => $this->portfolioImage,
                ":f" => $this->portfolioCategory,
                ":g" => '',
                ":h" => '',
                ":i" => '',
            ));
            if ($stmt){
            	move_uploaded_file($this->portfolioImageLocation, "../../../images/portfolio/".$this->portfolioImage);
                session_start();
                $_SESSION['message']="Porfolio data inserted successfully.";
                header('location:create.php');
            }
        }catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
	}

    public function index(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM `portfolios` ORDER BY id DESC";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            return $data;
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

	public function myOwnPortfolio(){
        try {
	        $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
	        $query = "SELECT * FROM `portfolios` WHERE user_id=$this->id";
	        $stmt = $pdo->prepare($query);
	        $stmt->execute();
	        $data = $stmt->fetchall();
	        return $data;
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function show(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM portfolios WHERE user_id=$this->user_id AND id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if(empty($data)){
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header("location:index.php");
            }else{
                return $data;
            }
        } catch(PDOException $e){
          echo 'Error: ' . $e->getMessage();
        }
    }
    public function updateWithImage(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "UPDATE portfolios SET `title`=:a,`description`=:b,`img`=:c,`category`=:d,`created_at`=:e,`updated_at`=:f,`deleted_at`=:g WHERE user_id = $this->user_id AND id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' =>$this->portfolioName,
                ':b' => $this->portfolioDescription,
                ':c' => $this->portfolioImage,
                ':d' => $this->portfolioCategory,
                ':e' => '',
                ":f" => '',
                ":g" => '',
            ));
            if ($stmt){
                $imagePrevious = "../../../images/portfolio/$this->previousDbImg";
                unlink($imagePrevious);

                move_uploaded_file($this->portfolioImageLocation, "../../../images/portfolio/".$this->portfolioImage);
                session_start();
                $_SESSION['message']="Protfolio data updated with image successfully.";
                header('location:create.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
    }
    public function updateWithoutImage(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "UPDATE portfolios SET `title`=:a,`description`=:b,`category`=:c,`created_at`=:d,`updated_at`=:e,`deleted_at`=:f WHERE user_id = $this->user_id AND id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' =>$this->portfolioName,
                ':b' => $this->portfolioDescription,
                ':c' => $this->portfolioCategory,
                ':d' => '',
                ":e" => '',
                ":f" => '',
            ));
            if ($stmt){
                session_start();
                $_SESSION['message']="Portfolios data Updated with out image successfully.";
                header('location:create.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
    }
    public function delete(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "DELETE FROM portfolios WHERE id = $this->id AND user_id=$this->user_id";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            if ($stmt=='') {
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header('location:index.php');

            }else{
                $imagePrevious = "../../../images/portfolio/$this->previousDbImg";
                unlink($imagePrevious);
                session_start();
                $_SESSION['message']="Data deleted permanently.";
                header('location:index.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    
}