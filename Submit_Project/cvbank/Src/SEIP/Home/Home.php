<?php
namespace App\SEIP\Home;
use PDO;
class Home{
    public $searchItem='';
    public function setData($data=''){
        if(array_key_exists('search',$data)){
           $this->searchItem = $data['search'];
        }
        return $this;
    }
     public function search(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM `users`
                left JOIN settings
                ON users.id=settings.user_id
                WHERE users.username LIKE '%$this->searchItem%' or settings.title LIKE '%$this->searchItem%'";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
/*
    public function search($noOfItems,$offset){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM `users`
                left JOIN settings
                ON users.id=settings.user_id
                WHERE users.username LIKE '%$this->searchItem%' or settings.title LIKE '%$this->searchItem%' LIMIT $noOfItems OFFSET $offset";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }*/
/*    public function getNumOfrows(){
        $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
        $query = "SELECT * FROM `users`
            left JOIN settings
            ON users.id=settings.user_id
            WHERE users.username LIKE '%$this->searchItem%' or settings.title LIKE '%$this->searchItem%'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = count($stmt->fetchAll());
        return $data;
    }*/
    public function getUserId(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM users WHERE username='$this->searchItem'";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            return $data;
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function showSkillData($userId){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM skills WHERE user_id=$userId";
            /*echo $query;
            die();*/
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            return $data;
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
    }
	public function showPortfolioData($userId){
		try {

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM portfolios WHERE user_id=$userId";
            /*echo $query;
            die();*/
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            return $data;
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
	}

    public function showPostData($userId){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM posts WHERE user_id=$userId";
            /*echo $query;
            die();*/
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            return $data;
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
    }
    public function showEducationData($userId){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM educations WHERE user_id=$userId";
            /*echo $query;
            die();*/
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            return $data;
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
    }    
    public function showExperienceData($userId){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM experiences WHERE user_id=$userId";
            /*echo $query;
            die();*/
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            return $data;
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
    }        
    public function showAwardData($userId){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM awards WHERE user_id=$userId";
            /*echo $query;
            die();*/
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            return $data;
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
    }            
    public function showAboutData($userId){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM abouts WHERE user_id=$userId";
        /*    echo $query;
            die();*/
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            return $data;
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
    }    
        public function showProfileData($userId){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            //$query = "SELECT * FROM abouts WHERE user_id=$userId";
            $query = "SELECT * FROM `users` left JOIN settings ON users.id=settings.user_id LEFT JOIN abouts ON users.id=abouts.user_id WHERE users.id=$userId OR settings.user_id=$userId OR abouts.user_id=$userId";
            /*echo $query;
            die();*/
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            return $data;
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
    }             
    public function showAboutHobby($userId){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM hobbies WHERE user_id=$userId";
            /*echo $query;
            die();*/
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            return $data;
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
    }
                   
    public function showAboutFact($userId){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM facts WHERE user_id=$userId";
            /*echo $query;
            die();*/
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            return $data;
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
    }

    public function showSettingDetails($userId){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM settings WHERE user_id=$userId";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            return $data;
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
    }
    public function showUserDetails($userId){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM users WHERE id=$userId";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            return $data;
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
    }

}