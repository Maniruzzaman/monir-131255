<?php
namespace App\SEIP\User;
use PDO;
class User{
    public $username = '';
    public $email = '';
    public $pdo = '';
    public $password = '';

    public function __construct(){
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
    }

    public function setData($data = ''){
        if (array_key_exists('username', $data)){
            $this->username = $data['username'];
        }
        if (array_key_exists('email', $data)){
            $this->email = $data['email'];
        }
        if (array_key_exists('password', $data)){
            $this->password = $data['password'];
        }
        return $this;
    }

    public function store(){
        try {
            $token = sha1($this->username);
            $query = "INSERT INTO `users` (`id`,`unique_id`, `username`, `email`, `password`,`token`, `is_active`,`user_role`,`created_at`,`updated_at`,`deleted_at`) VALUES (:id, :uniqid,:uname, :email, :pw, :tk,:isactv,:usrroll,:createat,:updateat,:deleteat)";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                ':id'      => null,
                ':uniqid'  => uniqid(),
                ':uname'   => $this->username,
                ':email'   => $this->email,
                ':pw'      => $this->password,
                ':tk'      => $token,
                'isactv'   => 1,
                'usrroll'  => 1,
                'createat' => '',
                'updateat' => '',
                'deleteat' => ''
                )
            );
            if ($stmt) {
                //echo "http://localhost/cvBank/Views/SEIP/varify/varify.php?token=$token";
               $msg = "<a href='http://dreambd.net/Views/SEIP/varify/varify.php?token=$token'></a>";
                mail($this->email, 'Signup mail',$msg);
                $_SESSION['message']="Signup successfully now go to your email for varification.";
                header('location: index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function userAvailability(){
        try {
            $query = "SELECT  * FROM `users` WHERE username = '$this->username' OR email = '$this->email'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function login(){
        try {
            $query = "SELECT  * FROM `users` WHERE (username = '$this->username' OR email = '$this->username') AND password = '$this->password'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if (!empty($data)) {
                if($data['is_active'] == 1){
                    $_SESSION['user_info'] = $data;
                    header('location:../../../Views/SEIP/Admin/index.php');
                }else{
                    $_SESSION['fail'] = "Please verify your email now you go to your email inbox ";
                header('location:index.php');
                }
            } else {
                $_SESSION['fail'] = "Invalid Username, Email Or Password";
                header('location:index.php');
            }

            return $data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }  
    public function getUserTokenInfo($token=''){
        try {
            $query = "SELECT  * FROM `users` WHERE token = '$token' ";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            return $data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    } 
    public function getVerify($userId=''){
        $query = "UPDATE users SET is_active=:value WHERE id=$userId";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute(array(
            ':value' => 1,
        ));
        if($stmt){
            $_SESSION['message'] = "Successfully varified.";
            header('location : ');
        }
    }

}

