<?php
namespace App\SEIP\Home;
use PDO;
class Home{
/*	public $id='';

	public function setData($data=''){
        if(array_key_exists('id',$data)){
           $this->id = $data['id'];
        }
        return $this;
	}*/
    public function showSkillData($userId){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM skills WHERE user_id=$userId";
            /*echo $query;
            die();*/
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            if(empty($data)){
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header("location:index.php");
            }else{
                return $data;
            }
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
    }
	public function showPortfolioData($userId){
		try {

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM portfolios WHERE user_id=$userId";
            /*echo $query;
            die();*/
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            if(empty($data)){
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header("location:index.php");
            }else{
                return $data;
            }
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
	}

    public function showPostData($userId){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM posts WHERE user_id=$userId";
            /*echo $query;
            die();*/
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            if(empty($data)){
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header("location:index.php");
            }else{
                return $data;
            }
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
    }
    public function showEducationData($userId){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM educations WHERE user_id=$userId";
            /*echo $query;
            die();*/
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            if(empty($data)){
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header("location:index.php");
            }else{
                return $data;
            }
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
    }    
    public function showExperienceData($userId){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM experiences WHERE user_id=$userId";
            /*echo $query;
            die();*/
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            if(empty($data)){
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header("location:index.php");
            }else{
                return $data;
            }
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
    }        
    public function showAwardData($userId){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM awards WHERE user_id=$userId";
            /*echo $query;
            die();*/
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            if(empty($data)){
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header("location:index.php");
            }else{
                return $data;
            }
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
    }            
    public function showAboutData($userId){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM abouts WHERE user_id=$userId";
            /*echo $query;
            die();*/
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if(empty($data)){
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header("location:index.php");
            }else{
                return $data;
            }
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
    }                
    public function showAboutHobby($userId){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM hobbies WHERE user_id=$userId";
            /*echo $query;
            die();*/
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            if(empty($data)){
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header("location:index.php");
            }else{
                return $data;
            }
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
    }
                   
    public function showAboutFact($userId){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM facts WHERE user_id=$userId";
            /*echo $query;
            die();*/
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            if(empty($data)){
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header("location:index.php");
            }else{
                return $data;
            }
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
    }

    public function showSettingDetails($userId){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM settings WHERE user_id=$userId";
            /*echo $query;
            die();*/
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if(empty($data)){
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header("location:index.php");
            }else{
                return $data;
            }
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
    }
}