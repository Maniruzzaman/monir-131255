<?php
namespace App\SEIP\Setting;
use PDO;
Class Setting{
	public $id='';
	public $userId='';
	public $previousDbImg='';
	public $settingName='';
	public $settingFullname='';
	public $settingDescription='';
	public $settingAddress='';
	public $settingImage='';
	public $settingImageLocation = '';

	public function setData($data=''){
		if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }        
		if(array_key_exists('user_id',$data)){
            $this->userId = $data['user_id'];
        }      
		if(array_key_exists('previous_db_img',$data)){
            $this->previousDbImg = $data['previous_db_img'];
        }
		if(array_key_exists('setting_name', $data)){
			$this->settingName = $data['setting_name'];
		}
		if(array_key_exists('fullname', $data)){
			$this->settingFullname = $data['fullname'];
		}
		if(array_key_exists('setting_description', $data)){
			$this->settingDescription = $data['setting_description'];
		}
		if(array_key_exists('setting_address', $data)){
			$this->settingAddress = $data['setting_address'];
		}	
		if(array_key_exists('setting_image', $data)){
			$this->settingImage = uniqid().time().$data['setting_image'];
		}	
		if(array_key_exists('setting_img_location', $data)){
			$this->settingImageLocation = $data['setting_img_location'];
		}			
		return $this;
	}

	public function store($userId){
		 try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
$query = "INSERT INTO `settings` (`id`,`user_id`,`title`,`fullname`,`description`,`address`,`featured_img`,`created_at`,`updated_at`,`deleted_at`) VALUES(:a,:b,:c,:d,:e,:f,:g,:h,:i,:j)";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' =>null,
                ':b' => $userId,
                ':c' => $this->settingName,
                ':d' => $this->settingFullname,
                ':e' => $this->settingDescription,
                ":f" => $this->settingAddress,
                ":g" => $this->settingImage,
                ":h" => '',
                ":i" => '',
                ":j" => '',
            ));
            if ($stmt){
            	move_uploaded_file($this->settingImageLocation, "../../../images/setting/".$this->settingImage);
                session_start();
                $_SESSION['message']="Settion data inserted successfully.";
                header('location:create.php');
            }
        }catch(PDOException $e) {
            echo 'Error: '. $e->getMessage();

        }
	}

	public function index(){
        try {
	        $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
	        $query = "SELECT * FROM `settings` ORDER BY id DESC";
	        $stmt = $pdo->prepare($query);
	        $stmt->execute();
	        $data = $stmt->fetchall();
	        return $data;
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function show(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM settings WHERE user_id=$this->userId AND id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if(empty($data)){
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header("location:index.php");
            }else{
                return $data;
            }
        } catch(PDOException $e){
          echo 'Error: ' . $e->getMessage();
        }
    }
    public function updateWithImage(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');

            $query = "UPDATE settings SET `title`=:a,`fullname`=:b,`description`=:c,`address`=:d,`featured_img`=:e,`created_at`=:f,`updated_at`=:g,`deleted_at`=:h WHERE user_id = $this->userId AND id=$this->id";

            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' =>$this->settingName,
                ':b' => $this->settingFullname,
                ':c' => $this->settingDescription,
                ':d' => $this->settingAddress,
                ':e' => $this->settingImage,
                ":f" => '',
                ":g" => '',
                ":h" => '',
            ));
            if ($stmt){
                $imagePrevious = "../../../images/setting/$this->previousDbImg";
                unlink($imagePrevious);

                move_uploaded_file($this->settingImageLocation, "../../../images/setting/".$this->settingImage);
                session_start();
                $_SESSION['message']="Setting data updated with image successfully.";
                header('location:create.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function updateWithoutImage(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "UPDATE settings SET `title`=:a,`fullname`=:b,`description`=:c,`address`=:d,`created_at`=:e,`updated_at`=:f,`deleted_at`=:g WHERE user_id = $this->userId AND id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' =>$this->settingName,
                ':b' => $this->settingFullname,
                ':c' => $this->settingDescription,
                ':d' => $this->settingAddress,
                ':e' => '',
                ":f" => '',
                ":g" => '',
            ));
            if ($stmt){
                session_start();
                $_SESSION['message']="Setting data Updated with out image successfully.";
                header('location:create.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
    }
    public function delete(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "DELETE FROM settings WHERE id = $this->id AND user_id=$this->userId";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            if ($stmt=='') {
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header('location:index.php');

            }else{
                $imagePrevious = "../../../images/setting/$this->previousDbImg";
                unlink($imagePrevious);
                session_start();
                $_SESSION['message']="Data deleted permanently.";
                header('location:index.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}