<?php
namespace App\SEIP\Resume\Education;
use PDO;
Class Education{
    public $id='';
    public $userId = '';
    public $educationName='';
    public $educationInstitute='';
    public $educationResult='';
    public $passingYear='';
    public $mainSubject='';
    public $educationBoard='';
    public $courseDuration='';

    public function setData($data=''){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('user_id',$data)){
            $this->userId = $data['user_id'];
        }
        if(array_key_exists('education_name',$data)){
            $this->educationName = $data['education_name'];
        }
        if(array_key_exists('education_institute',$data)){
            $this->educationInstitute = $data['education_institute'];
        }
        if(array_key_exists('education_result',$data)){
            $this->educationResult = $data['education_result'];
        }
        if(array_key_exists('passing_year',$data)){
            $this->passingYear = $data['passing_year'];
        }
        if(array_key_exists('main_subject',$data)){
            $this->mainSubject = $data['main_subject'];
        }
        if(array_key_exists('education_board',$data)){
            $this->educationBoard = $data['education_board'];
        }
        if(array_key_exists('course_duration',$data)){
            $this->courseDuration = $data['course_duration'];
        }
        return $this;
    }
    public function store($userId){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "INSERT INTO `educations` (`id`,`user_id`,`title`,`institute`,`result`,`passing_year`,`main_subject`,`education_board`,`course_duration`,`created_at`,`updated_at`,`deleted_at`) VALUES(:a,:b,:c,:d,:e,:f,:g,:h,:i,:j,:k,:l)";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' =>null,
                ':b' => $userId,
                ':c' => $this->educationName,
                ':d' => $this->educationInstitute,
                ':e' => $this->educationResult,
                ':f' => $this->passingYear,
                ':g' => $this->mainSubject,
                ':h'=>$this->educationBoard,
                ':i'=>$this->courseDuration,
                ':j'=>'',
                ':k'=>'',
                ':l'=>'',
            ));
            if ($stmt){
                session_start();
                $_SESSION['message']="Education data inserted successfully.";
                header('location:create.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
    }
    public function index(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM `educations` ORDER BY id DESC";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            return $data;
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function show(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM educations WHERE user_id=$this->userId AND id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if(empty($data)){
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header("location:index.php");
            }else{
                return $data;
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function update(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query ="UPDATE educations SET `title`=:a,`institute`=:b,`result`=:c,`passing_year`=:d,`main_subject`=:e,`education_board`=:f,`course_duration`=:g,`created_at`=:h,`updated_at`=:i,`deleted_at`=:j WHERE id = $this->id AND user_id=$this->userId";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' => $this->educationName,
                ':b' => $this->educationInstitute,
                ':c' => $this->educationResult,
                ':d' => $this->passingYear,
                ':e' => $this->mainSubject,
                ':f'=>$this->educationBoard,
                ':g'=>$this->courseDuration,
                ':h'=>'',
                ':i'=>'',
                ':j'=>'',
            ));
            if ($stmt){
                session_start();
                $_SESSION['message']="Education data updated successfully.";
                header('location:index.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
    }
    public function delete(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "DELETE FROM educations WHERE id = $this->id AND user_id = '$this->userId'";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            if ($stmt=='') {
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header('location:index.php');

            }else{
                session_start();
                $_SESSION['message']="Data deleted permanently.";
                header('location:index.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}