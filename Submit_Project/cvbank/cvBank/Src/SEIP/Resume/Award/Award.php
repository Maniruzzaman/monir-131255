<?php
namespace App\SEIP\Resume\Award;
use PDO;
Class Award{
    public $id='';
    public $userId = '';
    public $awardName='';
    public $awardOrganization='';
    public $awardDescription='';
    public $awardLocation='';
    public $awardDate='';

    public function setData($data=''){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('user_id',$data)){
            $this->userId = $data['user_id'];
        }
        if(array_key_exists('award_name',$data)){
            $this->awardName = $data['award_name'];
        }
        if(array_key_exists('organization_name',$data)){
            $this->awardOrganization = $data['organization_name'];
        }
        if(array_key_exists('award_description',$data)){
            $this->awardDescription = $data['award_description'];
        }
        if(array_key_exists('award_location',$data)){
            $this->awardLocation = $data['award_location'];
        }
        if(array_key_exists('award_date',$data)){
            $this->awardDate = $data['award_date'];
        }
        return $this;
    }
    public function store($userId){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "INSERT INTO `awards` (`id`,`user_id`,`title`,`organization`,`description`,`location`,`year`,`created_at`,`updated_at`,`deleted_at`) VALUES(:a,:b,:c,:d,:e,:f,:g,:h,:i,:j)";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' =>null,
                ':b' => $userId,
                ':c' => $this->awardName,
                ':d' => $this->awardOrganization,
                ':e' => $this->awardDescription,
                ':f' => $this->awardLocation,
                ':g' => $this->awardDate,
                ':h'=>'',
                ':i'=>'',
                ':j'=>'',
            ));
            if ($stmt){
                session_start();
                $_SESSION['message']="Award data inserted successfully.";
                header('location:create.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
    }
    public function index(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM `awards` ORDER BY id DESC";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            return $data;
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function show(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM awards WHERE user_id=$this->userId AND id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if(empty($data)){
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header("location:index.php");
            }else{
                return $data;
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function update(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');

            $query ="UPDATE awards SET `title`=:a,`organization`=:b,`description`=:c,`location`=:d,`year`=:e,`created_at`=:f,`updated_at`=:g,`deleted_at`=:h WHERE id = $this->id AND user_id=$this->userId";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' => $this->awardName,
                ':b' => $this->awardOrganization,
                ':c' => $this->awardDescription,
                ':d' => $this->awardLocation,
                ':e' => $this->awardDate,
                ':f'=>'',
                ':g'=>'',
                ':h'=>'',
            ));
            if ($stmt){
                session_start();
                $_SESSION['message']="Award data updated successfully.";
                header('location:index.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
    }
    public function delete(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "DELETE FROM awards WHERE id = $this->id AND user_id = '$this->userId'";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            if ($stmt=='') {
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header('location:index.php');

            }else{
                session_start();
                $_SESSION['message']="Data deleted permanently.";
                header('location:index.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}