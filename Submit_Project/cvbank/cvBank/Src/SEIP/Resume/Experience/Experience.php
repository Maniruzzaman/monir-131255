<?php
namespace App\SEIP\Resume\Experience;
use PDO;
Class Experience{
    public $id='';
    public $userId = '';
    public $designationName='';
    public $companyName='';
    public $startingDate='';
    public $endingDate='';
    public $companyLocation='';

    public function setData($data=''){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('user_id',$data)){
            $this->userId = $data['user_id'];
        }
        if(array_key_exists('designation_name',$data)){
            $this->designationName = $data['designation_name'];
        }
        if(array_key_exists('company_name',$data)){
            $this->companyName = $data['company_name'];
        }
        if(array_key_exists('starting_date',$data)){
            $this->startingDate = $data['starting_date'];
        }
        if(array_key_exists('ending_date',$data)){
            $this->endingDate = $data['ending_date'];
        }
        if(array_key_exists('company_location',$data)){
            $this->companyLocation = $data['company_location'];
        }
        return $this;
    }
    public function store($userId){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "INSERT INTO `experiences` (`id`,`user_id`,`designation`,`company_name`,`start_date`,`end_date`,`company_location`,`created_at`,`updated_at`,`deleted_at`) VALUES(:a,:b,:c,:d,:e,:f,:g,:h,:i,:j)";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' =>null,
                ':b' => $userId,
                ':c' => $this->designationName,
                ':d' => $this->companyName,
                ':e' => $this->startingDate,
                ':f' => $this->endingDate,
                ':g' => $this->companyLocation,
                ':h'=>'',
                ':i'=>'',
                ':j'=>'',
            ));
            if ($stmt){
                session_start();
                $_SESSION['message']="Experence data inserted successfully.";
                header('location:create.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
    }
    public function index(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM `experiences` ORDER BY id DESC";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            return $data;
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function show(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM experiences WHERE user_id=$this->userId AND id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if(empty($data)){
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header("location:index.php");
            }else{
                return $data;
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function update(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query ="UPDATE experiences SET `designation`=:a,`company_name`=:b,`start_date`=:c,`end_date`=:d,`company_location`=:e,`created_at`=:f,`updated_at`=:g,`deleted_at`=:h WHERE id = $this->id AND user_id=$this->userId";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' => $this->designationName,
                ':b' => $this->companyName,
                ':c' => $this->startingDate,
                ':d' => $this->endingDate,
                ':e' => $this->companyLocation,
                ':f'=>'',
                ':g'=>'',
                ':h'=>'',
            ));
            if ($stmt){
                session_start();
                $_SESSION['message']="Experience data updated successfully.";
                header('location:index.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
    }
    public function delete(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "DELETE FROM experiences WHERE id = $this->id AND user_id = '$this->userId'";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            if ($stmt=='') {
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header('location:index.php');

            }else{
                session_start();
                $_SESSION['message']="Data deleted permanently.";
                header('location:index.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}