<?php
namespace App\SEIP\Post;
use PDO;
Class Post{
    public $id='';
    public $user_id='';
    public $previousDbImg='';
    public $postName='';
    public $postDescription='';
    public $postImageLocation = '';
    public $postImage='';
    public $postCategory='';
    public $postTag='';

    public function setData($data=''){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('user_id',$data)){
            $this->user_id = $data['user_id'];
        }
        if(array_key_exists('previous_db_img',$data)){
            $this->previousDbImg = $data['previous_db_img'];
        }
        if(array_key_exists('post_name', $data)){
            $this->postName = $data['post_name'];
        }
        if(array_key_exists('post_description', $data)){
            $this->postDescription = $data['post_description'];
        }
        if(array_key_exists('post_img_location', $data)){
            $this->postImageLocation = $data['post_img_location'];
        }
        if(array_key_exists('post_image', $data)){
            $this->postImage = uniqid().time().$data['post_image'];
        }
        if(array_key_exists('post_category', $data)){
            $this->postCategory = $data['post_category'];
        }
        if(array_key_exists('post_tag', $data)){
            $this->postTag = serialize($data['post_tag']);
        }
        return $this;
    }

    public function store($userId){
        try {
            /*echo $this->postName."<br/>";
            echo $this->postDescription."<br/>";
            echo $this->postTag."<br/>";
            echo $this->postCategory."<br/>";
            echo $this->postImage."<br/>";
            die();*/

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "INSERT INTO `posts` (`id`,`user_id`,`title`,`description`,`tags`,`categories`,`img`,`created_at`,`updated_at`,`deleted_at`) VALUES(:a,:b,:c,:d,:e,:f,:g,:h,:i,:j)";
           /* echo $query;
            die();*/
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' =>null,
                ':b' => $userId,
                ':c' => $this->postName,
                ':d' => $this->postDescription,
                ':e' => $this->postTag,
                ":f" => $this->postCategory,
                ":g" => $this->postImage,
                ":h" => date("Y-m-d"),
                ":i" => '',
                ":j" => ''
            ));
            if ($stmt){
                move_uploaded_file($this->postImageLocation,"../../../images/post/".$this->postImage);
                session_start();
                $_SESSION['message']="Post data inserted successfully.";
                header('location:create.php');
            }
        }catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
    }

    public function index(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM `posts` ORDER BY id DESC";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            return $data;
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
   public function show(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM posts WHERE user_id=$this->user_id AND id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if(empty($data)){
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header("location:index.php");
            }else{
                return $data;
            }
        } catch(PDOException $e){
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function updateWithImage(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "UPDATE posts SET `title`=:a,`description`=:b,`tags`=:c,`categories`=:d,`img`=:e,`created_at`=:f,`updated_at`=:g,`deleted_at`=:h WHERE user_id = $this->user_id AND id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' =>$this->postName,
                ':b' => $this->postDescription,
                ':c' => $this->postTag,
                ':d' => $this->postCategory,
                ':e' => $this->postImage,
                ":f" => '',
                ":g" => '',
                ":h" => '',
            ));
            if ($stmt){
                $imagePrevious = "../../../images/post/$this->previousDbImg";
                unlink($imagePrevious);

                move_uploaded_file($this->postImageLocation, "../../../images/post/".$this->postImage);
                session_start();
                $_SESSION['message']="Post data updated with image successfully.";
                header('location:create.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
    }
    public function updateWithoutImage(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "UPDATE posts SET `title`=:a,`description`=:b,`tags`=:c,`categories`=:d,`created_at`=:e,`updated_at`=:f,`deleted_at`=:g WHERE user_id = $this->user_id AND id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' =>$this->postName,
                ':b' => $this->postDescription,
                ':c' => $this->postTag,
                ':d' => $this->postCategory,
                ":e" => '',
                ":f" => '',
                ":g" => '',
            ));
            if ($stmt){
                session_start();
                $_SESSION['message']="Post data Updated with out image successfully.";
                header('location:create.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
    }
    public function delete(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "DELETE FROM posts WHERE id = $this->id AND user_id=$this->user_id";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            if ($stmt=='') {
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header('location:index.php');

            }else{
                $imagePrevious = "../../../images/post/$this->previousDbImg";
                unlink($imagePrevious);
                session_start();
                $_SESSION['message']="Data deleted permanently.";
                header('location:index.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}