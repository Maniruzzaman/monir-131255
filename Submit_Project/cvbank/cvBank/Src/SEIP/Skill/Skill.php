<?php
namespace App\SEIP\Skill;
use PDO;
Class Skill{
    public $id='';
    public $userId = '';
    public $skillTitle='';
    public $skillDescription='';
    public $skillLevel='';
    public $skillExperence='';
    public $skillArea='';
	public function setData($data=''){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('user_id',$data)){
            $this->userId = $data['user_id'];
        }
        if(array_key_exists('skill_title',$data)){
            $this->skillTitle = $data['skill_title'];
        }
        if(array_key_exists('skill_description',$data)){
            $this->skillDescription = $data['skill_description'];
        }
        if(array_key_exists('Skill_level',$data)){
            $this->skillLevel = $data['Skill_level'];
        }
        if(array_key_exists('Skill_experience',$data)){
            $this->skillExperence = $data['Skill_experience'];
        }
        if(array_key_exists('skill_area',$data)){
            $this->skillArea = serialize($data['skill_area']);
        }
        return $this;
    }
    public function store($userId){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "INSERT INTO `skills` (`id`,`user_id`,`title`,`description`,`level`,`experience`,`experience_area`,`created_at`,`updated_at`,`deleted_at`) VALUES(:a,:b,:c,:d,:e,:f,:g,:h,:i,:j)";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' =>null,
                ':b' => $userId,
                ':c' => $this->skillTitle,
                ':d' => $this->skillDescription,
                ':e' => $this->skillLevel,
                ':f' => $this->skillExperence,
                ':g' => $this->skillArea,
                ':h'=>'',
                ':i'=>'',
                ':j'=>'',
            ));
            if ($stmt){
                session_start();
                $_SESSION['message']="Skill data inserted successfully.";
                header('location:create.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
    }
    public function index(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM `skills` ORDER BY id DESC";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            return $data;
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function show(){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM skills WHERE user_id=$this->userId AND id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if(empty($data)){
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header("location:index.php");
            }else{
                return $data;
            }
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
        }
    }
    public function update(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = 
            "UPDATE skills SET `title`=:a,`description`=:b,`level`=:c,`experience`=:d,`experience_area`=:e,`created_at`=:f,`updated_at`=:g,`deleted_at`=:h WHERE id = $this->id AND user_id=$this->userId";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' =>$this->skillTitle,
                ':b' => $this->skillDescription,
                ':c' => $this->skillLevel,
                ':d' => $this->skillExperence,
                ':e' => $this->skillArea,
                ':f' => '',
                ':g' => '',
                ':h'=> '',
            ));
            if ($stmt){
                session_start();
                $_SESSION['message']="Skill data updated successfully.";
                header('location:index.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
    }
    public function delete(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "DELETE FROM skills WHERE id = $this->id AND user_id = '$this->userId'";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            if ($stmt=='') {
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header('location:index.php');

            }else{
                session_start();
                $_SESSION['message']="Data deleted permanently.";
                header('location:index.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}