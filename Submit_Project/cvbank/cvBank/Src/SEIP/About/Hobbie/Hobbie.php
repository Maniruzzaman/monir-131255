<?php
namespace App\SEIP\About\Hobbie;
use PDO;
Class Hobbie{
    private $id='';
    private $userId='';
    public  $previousDbImg='';
    private $hobbyName='';
    private $hobbyDescription='';
    private $hobbyImage='';
    private $hobbyImageLocation='';
    public function setData($data=''){
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }
        if(array_key_exists('user_id', $data)){
            $this->userId = $data['user_id'];
        }
        if(array_key_exists('previous_db_img', $data)){
            $this->previousDbImg = $data['previous_db_img'];
        }
        if(array_key_exists('hobby_name', $data)){
            $this->hobbyName = $data['hobby_name'];
        }
        if(array_key_exists('hobby_description', $data)){
            $this->hobbyDescription = $data['hobby_description'];
        }
        if(array_key_exists('hobby_image', $data)){
            $this->hobbyImage = uniqid().time().$data['hobby_image'];
        }
        if(array_key_exists('hobby_img_location', $data)){
            $this->hobbyImageLocation = $data['hobby_img_location'];
        }
        return $this;
    }
    public function store($userId){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "INSERT INTO `hobbies`(`id`,`user_id`,`title`,`description`,`img`,`created_at`,`updated_at`,`deleted_at`) VALUES(:a,:b,:c,:d,:e,:f,:g,:h)";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' =>null,
                ':b' => $userId,
                ':c' => $this->hobbyName,
                ':d' => $this->hobbyDescription,
                ':e' => $this->hobbyImage,
                ':f' => '',
                ':g' => '',
                ':h' => '',
            ));
            if ($stmt){
                move_uploaded_file($this->hobbyImageLocation, "../../../../images/about/hobby/".$this->hobbyImage);
                session_start();
                $_SESSION['message']="Hobby data inserted successfully.";
                header('location:create.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
    }
    public function index(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM `hobbies` ORDER BY id DESC";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            return $data;
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function show(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM hobbies WHERE user_id=$this->userId AND id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if(empty($data)){
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header("location:index.php");
            }else{
                return $data;
            }
        } catch(PDOException $e){
          echo 'Error: ' . $e->getMessage();
        }
    }
    public function updateWithImage(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "UPDATE hobbies SET `title`=:a,`description`=:b,`img`=:c,`created_at`=:d,`updated_at`=:e,`deleted_at`=:f WHERE user_id = $this->userId AND id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' =>$this->hobbyName,
                ':b' => $this->hobbyDescription,
                ':c' => $this->hobbyImage,
                ':d' => '',
                ':e' => '',
                ":f" => '',
            ));
            if ($stmt){
                $imagePrevious = "../../../../images/about/hobby/$this->previousDbImg";
                unlink($imagePrevious);

               move_uploaded_file($this->hobbyImageLocation, "../../../../images/about/hobby/".$this->hobbyImage);
                session_start();
                $_SESSION['message']="Hobby data updated with image successfully.";
                header('location:create.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
    }
    public function updateWithoutImage(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "UPDATE hobbies SET `title`=:a,`description`=:b,`created_at`=:c,`updated_at`=:d,`deleted_at`=:e WHERE user_id = $this->userId AND id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' =>$this->hobbyName,
                ':b' => $this->hobbyDescription,
                ':c' => '',
                ':d' => '',
                ':e' => '',
            ));
            if ($stmt){
                session_start();
                $_SESSION['message']="Portfolios data Updated with out image successfully.";
                header('location:create.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
    }
    public function delete(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "DELETE FROM hobbies WHERE id = $this->id AND user_id=$this->userId";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            if ($stmt=='') {
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header('location:index.php');

            }else{
                $imagePrevious = "../../../../images/about/hobby/$this->previousDbImg";
                unlink($imagePrevious);
                session_start();
                $_SESSION['message']="Data deleted permanently.";
                header('location:index.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    

}