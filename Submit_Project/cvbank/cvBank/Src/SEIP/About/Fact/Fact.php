<?php
namespace App\SEIP\About\Fact;
use PDO;
Class Fact{
    private $id='';
    private $userId='';
    public  $previousDbImg='';
    private $factName='';
    private $factCounter='';
    private $factImage='';
    private $factImageLocation='';
    public function setData($data=''){
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }
        if(array_key_exists('user_id', $data)){
            $this->userId = $data['user_id'];
        }
        if(array_key_exists('previous_db_img', $data)){
            $this->previousDbImg = $data['previous_db_img'];
        }
        if(array_key_exists('fact_name', $data)){
            $this->factName = $data['fact_name'];
        }
        if(array_key_exists('fact_count', $data)){
            $this->factCounter = $data['fact_count'];
        }
        if(array_key_exists('fact_image', $data)){
            $this->factImage = uniqid().time().$data['fact_image'];
        }
        if(array_key_exists('fact_img_location', $data)){
            $this->factImageLocation = $data['fact_img_location'];
        }
        return $this;
    }
    public function store($userId){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "INSERT INTO `facts`(`id`,`user_id`,`title`,`no_of_items`,`img`,`created_at`,`updated_at`,`deleted_at`) VALUES(:a,:b,:c,:d,:e,:f,:g,:h)";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' =>null,
                ':b' => $userId,
                ':c' => $this->factName,
                ':d' => $this->factCounter,
                ':e' => $this->factImage,
                ':f' => '',
                ':g' => '',
                ':h' => '',
            ));
            if ($stmt){
                move_uploaded_file($this->factImageLocation, "../../../../images/about/fact/".$this->factImage);
                session_start();
                $_SESSION['message']="Fact data inserted successfully.";
                header('location:create.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
    }
    public function index(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM `facts` ORDER BY id DESC";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
            return $data;
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function show(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "SELECT * FROM facts WHERE user_id=$this->userId AND id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if(empty($data)){
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header("location:index.php");
            }else{
                return $data;
            }
        } catch(PDOException $e){
          echo 'Error: ' . $e->getMessage();
        }
    }

    public function updateWithImage(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "UPDATE facts SET `title`=:a,`no_of_items`=:b,`img`=:c,`created_at`=:d,`updated_at`=:e,`deleted_at`=:f WHERE user_id = $this->userId AND id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' => $this->factName,
                ':b' => $this->factCounter,
                ':c' => $this->factImage,
                ':d' => '',
                ':e' => '',
                ":f" => '',
            ));
            if ($stmt){
                $imagePrevious = "../../../../images/about/fact/$this->previousDbImg";
                unlink($imagePrevious);

               move_uploaded_file($this->factImageLocation, "../../../../images/about/fact/".$this->factImage);
                session_start();
                $_SESSION['message']="Fact data updated with image successfully.";
                header('location:create.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
    }
    public function updateWithoutImage(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "UPDATE facts SET `title`=:a,`no_of_items`=:b,`created_at`=:c,`updated_at`=:d,`deleted_at`=:e WHERE user_id = $this->userId AND id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' =>$this->factName,
                ':b' => $this->factCounter,
                ':c' => '',
                ':d' => '',
                ':e' => '',
            ));
            if ($stmt){
                session_start();
                $_SESSION['message']="Fact data Updated without image successfully.";
                header('location:create.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
    }
    public function delete(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
            $query = "DELETE FROM facts WHERE id = $this->id AND user_id=$this->userId";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            if ($stmt=='') {
                session_start();
                $_SESSION['message']="Oop invalid input.";
                header('location:index.php');

            }else{
                $imagePrevious = "../../../../images/about/fact/$this->previousDbImg";
                unlink($imagePrevious);
                session_start();
                $_SESSION['message']="Data deleted permanently.";
                header('location:index.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    

}