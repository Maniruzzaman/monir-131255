<?php
/*echo "<pre>";
print_r($_POST);
print_r($_FILES);
die();*/
session_start();
if (empty($_SESSION['user_info'])) {
    $_SESSION['fail'] = "Sory! Your are not authorized to access this page";
    header('location:../index.php');
}
include_once "../../../../vendor/autoload.php";
use App\SEIP\About\Hobbie\Hobbie;
$obj = new Hobbie();


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(!empty($_POST['hobby_name'])){
        if(!empty($_POST['hobby_description'])){
            if(!empty($_FILES['hobby_image']['name'])){
                $image_name = $_FILES['hobby_image']['name'];
                $image_type = $_FILES['hobby_image']['type'];
                $image_tmp_location  = $_FILES['hobby_image']['tmp_name'];
                $image_size = $_FILES['hobby_image']['size'];
                $my_image_extension = strtolower(end(explode(".",$image_name)));
                $required_formate = array('jpg','jpeg','png','gif');
                if(in_array($my_image_extension, $required_formate)){
                    $_POST['hobby_image'] = $image_name;
                    $_POST['hobby_img_location'] = $image_tmp_location;
                    $_POST['hobby_name'] = filter_var($_POST['hobby_name'],FILTER_SANITIZE_STRING);
                    $_POST['hobby_description'] = filter_var($_POST['hobby_description'],FILTER_SANITIZE_STRING);
                    $obj->setData($_POST)->store($_SESSION['user_info']['id']);
                }else{
                    session_start();
                    $_SESSION['message'] = "Image formate should be jpg, png, gif,jpeg.";
                    header('location: create.php');
                }
            }else{
                session_start();
                $_SESSION['message'] = "Hobby image can't be empty.";
                header('location: create.php');
            }

        }else{
            session_start();
            $_SESSION['message'] = "Hobby description can't be empty.";
            header('location: create.php');
        }
    }else{
        session_start();
        $_SESSION['message'] = "Hobby title can't be empty.";
        header('location: create.php');
    }
}


