<?php
/*echo "<pre>";
print_r($_POST);
die();*/
include "../../../../vendor/autoload.php";
use App\SEIP\About\AboutBio\AboutBio;
$obj = new AboutBio();

if(!empty($_POST['bio_title'])){
		if(!empty($_POST['bio_phone'])){
			if (!empty($_POST['bio_description'])) {
				$_POST['bio_title'] = filter_var($_POST['bio_title'],FILTER_SANITIZE_STRING);
				$_POST['bio_phone'] = filter_var($_POST['bio_phone'],FILTER_SANITIZE_STRING);
				$_POST['bio_description'] = filter_var($_POST['bio_description'],FILTER_SANITIZE_STRING);
				
				$obj->setData($_POST)->update();
			}else{
				session_start();
				$_SESSION['message'] = "Bio description can't be empty.";
				header('location: create.php');
			}
		}else{
			session_start();
			$_SESSION['message'] = "Phone number field can't be empty.";
			header('location: create.php');
		}
		
	}else{
		session_start();
		$_SESSION['message'] = "About bio title can't be empty.";
		header('location: create.php');
	}