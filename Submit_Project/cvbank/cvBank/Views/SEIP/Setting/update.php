<?php
/*echo "<pre>";
print_r($_POST);
print_r($_FILES);
die();*/
session_start();
if (empty($_SESSION['user_info'])) {
    $_SESSION['fail'] = "Sory! Your are not authorized to access this page";
    header('location:../index.php');
}
include_once "../../../vendor/autoload.php";
use App\SEIP\Setting\Setting;
$obj = new Setting(); 


if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	if(!empty($_POST['setting_name'])){
		if(!empty($_POST['fullname'])){
			if(!empty($_POST['setting_description'])){	
				if(!empty($_POST['setting_address'])){

		    	if(!empty($_FILES['setting_image']['name'])){

					$image_name = $_FILES['setting_image']['name'];
					$image_type = $_FILES['setting_image']['type'];
					$image_tmp_location  = $_FILES['setting_image']['tmp_name'];
					$image_size = $_FILES['setting_image']['size'];
					$my_image_extension = strtolower(end(explode(".",$image_name)));
					$required_formate = array('jpg','jpeg','png','gif');
		    		if(in_array($my_image_extension, $required_formate)){
		    			
		    			$_POST['setting_image'] = $image_name;		    			
						$_POST['setting_img_location'] = $image_tmp_location;

						$_POST['setting_name'] = filter_var($_POST['setting_name'],FILTER_SANITIZE_STRING);
						$_POST['fullname'] = filter_var($_POST['fullname'],FILTER_SANITIZE_STRING);
						$_POST['setting_description'] = filter_var($_POST['setting_description'],FILTER_SANITIZE_STRING);	
						$_POST['setting_address'] = filter_var($_POST['setting_address'],FILTER_SANITIZE_STRING);
						$obj->setData($_POST)->updateWithImage();		
					}else{
					session_start();
				    $_SESSION['message'] = "Image formate should be jpg, png, gif,jpeg.";
				    header('location: create.php');
					}

				}else{
					$_POST['setting_name'] = filter_var($_POST['setting_name'],FILTER_SANITIZE_STRING);
					$_POST['fullname'] = filter_var($_POST['fullname'],FILTER_SANITIZE_STRING);
					$_POST['setting_description'] = filter_var($_POST['setting_description'],FILTER_SANITIZE_STRING);
					$_POST['setting_address'] = filter_var($_POST['setting_address'],FILTER_SANITIZE_STRING);
					$obj->setData($_POST)->updateWithoutImage();
				}
				}else{
					session_start();
				    $_SESSION['message'] = "Address can't be empty.";
				    header('location: create.php');
				}			
			}else{
				session_start();
				$_SESSION['message'] = "Description can't be empty.";
				header('location: create.php');
			}
		}else{
			session_start();
			$_SESSION['message'] = "Fullname can't be empty.";
			header('location: create.php');
		}
	}else{
		session_start();
		$_SESSION['message'] = "Settion name can't be empty.";
		header('location: create.php');
	}
}