<?php
/*echo "<pre>";
print_r($_POST);
die();*/
session_start();
if (empty($_SESSION['user_info'])) {
    $_SESSION['fail'] = "Sory! Your are not authorized to access this page";
    header('location:../index.php');
}
include_once "../../../../vendor/autoload.php";
use App\SEIP\Resume\Award\Award;
$obj = new Award();


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(!empty($_POST['award_name'])){
        if(!empty($_POST['organization_name'])){
            if(!empty($_POST['award_description'])){
                if(!empty($_POST['award_location'])){
                    if(!empty($_POST['award_date'])){
                        $_POST['award_name'] = filter_var($_POST['award_name'],FILTER_SANITIZE_STRING);
                        $_POST['organization_name'] = filter_var($_POST['organization_name'],FILTER_SANITIZE_STRING);
                        $_POST['award_description'] = filter_var($_POST['award_description'],FILTER_SANITIZE_STRING);
                        $obj->setData($_POST)->store($_SESSION['user_info']['id']);
                    }else{
                        session_start();
                        $_SESSION['message'] = "Award date can't be empty.";
                        header('location: create.php');
                    }
                }else{
                    session_start();
                    $_SESSION['message'] = "Award location can't be empty.";
                    header('location: create.php');
                }
            }else{
                session_start();
                $_SESSION['message'] = "Award Description can't be empty.";
                header('location: create.php');
            }
        }else{
            session_start();
            $_SESSION['message'] = "Award organization name can't be empty.";
            header('location: create.php');
        }
    }else{
        session_start();
        $_SESSION['message'] = "Award title can't be empty.";
        header('location: create.php');
    }
}


