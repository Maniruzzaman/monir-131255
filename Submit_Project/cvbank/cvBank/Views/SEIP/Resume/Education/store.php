<?php
session_start();
if (empty($_SESSION['user_info'])) {
    $_SESSION['fail'] = "Sory! Your are not authorized to access this page";
    header('location:../index.php');
}
include_once "../../../../vendor/autoload.php";
use App\SEIP\Resume\Education\Education;
$obj = new Education();


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(!empty($_POST['education_name'])){
        if(!empty($_POST['education_institute'])){
            if(!empty($_POST['education_result'])){
                if(!empty($_POST['passing_year'])){
                    if(!empty($_POST['main_subject'])){
                        if(!empty($_POST['education_board'])){
                            if(!empty($_POST['course_duration'])){
                                $_POST['education_name'] = filter_var($_POST['education_name'],FILTER_SANITIZE_STRING);
                                $_POST['education_institute'] = filter_var($_POST['education_institute'],FILTER_SANITIZE_STRING);
                                $_POST['education_result'] = filter_var($_POST['education_result'],FILTER_SANITIZE_STRING);
                                $_POST['course_duration'] = filter_var($_POST['course_duration'],FILTER_SANITIZE_STRING);
                                $obj->setData($_POST)->store($_SESSION['user_info']['id']);
                            }else{
                                session_start();
                                $_SESSION['message'] = "Course duration can't be empty.";
                                header('location: create.php');
                            }
                        }else{
                            session_start();
                            $_SESSION['message'] = "Education board can't be empty.";
                            header('location: create.php');
                        }
                    }else{
                        session_start();
                        $_SESSION['message'] = "Main subject can't be empty.";
                        header('location: create.php');
                    }
                }else{
                    session_start();
                    $_SESSION['message'] = "Passing year can't be empty.";
                    header('location: create.php');
                }
            }else{
                session_start();
                $_SESSION['message'] = "Education result can't be empty.";
                header('location: create.php');
            }
        }else{
            session_start();
            $_SESSION['message'] = "Education institution field can't be empty.";
            header('location: create.php');
        }
    }else{
        session_start();
        $_SESSION['message'] = "Education name can't be empty.";
        header('location: create.php');
    }
}


