<?php
session_start();
if (empty($_SESSION['user_info'])) {
    $_SESSION['fail'] = "Sory! Your are not authorized to access this page";
    header('location:../index.php');
}
include_once "../../../vendor/autoload.php";
use App\SEIP\Post\Post;
$obj = new Post();


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(!empty($_POST['post_name'])){
        if(!empty($_POST['post_description'])){
            if(!empty($_POST['post_category'])){
                if(!empty($_POST['post_tag'])){
                    if(!empty($_FILES['post_image']['name'])){

                        $image_name = $_FILES['post_image']['name'];
                        $image_type = $_FILES['post_image']['type'];
                        $image_tmp_location  = $_FILES['post_image']['tmp_name'];
                        $image_size = $_FILES['post_image']['size'];
                        $my_image_extension = strtolower(end(explode(".",$image_name)));
                        $required_formate = array('jpg','jpeg','png','gif');

                        if(in_array($my_image_extension, $required_formate)){
                            $_POST['post_image'] = $image_name;
                            $_POST['post_img_location'] = $image_tmp_location;
                            $_POST['post_image'] = filter_var($_POST['post_image'],FILTER_SANITIZE_STRING);
                            $_POST['post_description'] = filter_var($_POST['post_description'],FILTER_SANITIZE_STRING);

                            $obj->setData($_POST)->updateWithImage();
                        }else{
                            session_start();
                            $_SESSION['message'] = "Image formate should be jpg, png, gif,jpeg.";
                            header('location: create.php');
                        }
                    }else{
                        $_POST['post_image'] = filter_var($_POST['post_image'],FILTER_SANITIZE_STRING);
                        $_POST['post_description'] = filter_var($_POST['post_description'],FILTER_SANITIZE_STRING);
                        $obj->setData($_POST)->updateWithoutImage();
                    }
                }else{
                    session_start();
                    $_SESSION['message'] = "Post category can't be empty.";
                    header('location: create.php');
                }
            }else{
                session_start();
                $_SESSION['message'] = "Post category can't be empty.";
                header('location: create.php');
            }
        }else{
            session_start();
            $_SESSION['message'] = "Post description can't be empty.";
            header('location: create.php');
        }
    }else{
        session_start();
        $_SESSION['message'] = "Post name can't be empty.";
        header('location: create.php');
    }
}

