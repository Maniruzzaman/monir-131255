<?php

session_start();
if (empty($_SESSION['user_info'])) {
    $_SESSION['fail'] = "Sory! Your are not authorized to access this page";
    header('location:../index.php');
}
include_once "../../../vendor/autoload.php";
use App\SEIP\Service\Service;
$obj = new Service(); 

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	    if(!empty($_POST['service_name'])){	    	
		    if(!empty($_POST['service_description'])){
		    	if(!empty($_FILES['service_img']['name'])){


					$image_name = $_FILES['service_img']['name'];
					$image_type = $_FILES['service_img']['type'];
					$image_tmp_location  = $_FILES['service_img']['tmp_name'];
					$image_size = $_FILES['service_img']['size'];
					$my_image_extension = strtolower(end(explode(".",$image_name)));
					$required_formate = array('jpg','jpeg','png','gif');

		    		if(in_array($my_image_extension, $required_formate)){
						if($image_size>20000){

							$_POST['service_img'] = $image_name;
							$_POST['service_img_location'] = $image_tmp_location;
							$_POST['service_name'] = filter_var($_POST['service_name'],FILTER_SANITIZE_STRING);
							$_POST['service_description'] = filter_var($_POST['service_description'],FILTER_SANITIZE_STRING);
							$obj->setData($_POST)->updateWithImage();
							
						}else{
							session_start();
						    $_SESSION['message'] = "Image size should be 2mb .";
						    header('location: create.php');
						}
					}else{
						session_start();
					    $_SESSION['message'] = "Image formate should be jpg, png, gif,jpeg.";
					    header('location: create.php');
					}


				}else{
					$_POST['service_name'] = filter_var($_POST['service_name'],FILTER_SANITIZE_STRING);
					$_POST['service_description'] = filter_var($_POST['service_description'],FILTER_SANITIZE_STRING);
					$obj->setData($_POST)->updateWithoutImage();
				}
			}else{
		    session_start();
		    $_SESSION['message'] = "Service description can't be empty.";
		    header('location: create.php');
		}
		}else{
	    session_start();
	    $_SESSION['message'] = "Service name can't be empty.";
	    header('location: create.php');
	}
}