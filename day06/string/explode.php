<?php
$str = "Hello world. It's a beautiful day.";
print_r (explode(" ",$str));

echo "<br>";
//2nd test
$str = 'one,two,three,four';

// zero limit
print_r(explode(',',$str,0));

// positive limit
print_r(explode(',',$str,2));

// negative limit
print_r(explode(',',$str,-1));