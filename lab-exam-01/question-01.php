<?php

//is_array
echo "Answer to the question is_array"."<br>";

$yes = array('this' , 'is' , 'an array');

echo is_array($yes) ? 'Array' : 'not an Array';
echo "\n";

$no = 'this is a string';

echo is_array($no) ? 'Array' : 'not an Array'. "<br>";

//is_null
echo "Answer to the question is_null"."<br>";

error_reporting(E_ALL);

$foo = NULL;
var_dump(is_null($inexistent), is_null($foo));

//print_r
echo "<br>";
echo "Answer to the question print_r"."<br>";

$a = array ('a' => 'apple', 'b' => 'banana', 'c' => array ('x', 'y', 'z'));
print_r ($a);

echo "<br>";

//Unset
echo "Answer to the question unset"."<br>";
function foo(&$bar)
{
    unset($bar);
    $bar = "blah";
}

$bar = 'something';
echo "$bar\n";

foo($bar);
echo "$bar\n";


