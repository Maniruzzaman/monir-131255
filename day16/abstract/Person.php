<?php


abstract class Person
{
    public abstract function getName();

    protected abstract function getNumberOfStudent();
}


class Peoples extends Person
{
    public function getName()
    {
        echo "This output is getName subClass.";
    }

    public function getNumberOfStudent()
    {
        return "This output is getNumberOfStudent subClass";
    }
}