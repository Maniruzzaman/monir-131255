<?php
echo '0:    ' .(boolval(0) ? 'true' : 'false' )."\n";
echo "<br>";
echo '42:   ' .(boolval(42) ? 'true' : 'false' )."\n";

echo "<br><br>";
echo "..................empty...................";
echo "<br><br>";
$var = 0;

if (empty($var)) {
    echo '$var is either 0, empty, or not set at all';
}


if (isset($var)) {
    echo '$var is set even though it is empty';
}
echo "<br><br>";
echo "..................gettype...................";
echo "<br><br>";

$data = array(1, 1., NULL, new stdClass, 'foo');

foreach ($data as $value) {
    echo gettype($value), "\n";
}

echo "<br><br>";
echo "..................is_array...................";
echo "<br><br>";

$yes = array('this' , 'is' , 'an array');

echo is_array($yes) ? 'Array' : 'not an Array';
echo "\n";

$no = 'this is a string';

echo is_array($no) ? 'Array' : 'not an Array';


echo "<br><br>";
echo "..................in_int...................";
echo "<br><br>";

$values = array(23, "23", 23.5, "23.5", null, true, false);
foreach ($values as $value) {
    echo "in_int(";
    var_export($value);
    echo ") = ";
    var_dump(is_int($value));
}

echo "<br><br>";
echo "..................is_null...................";
echo "<br><br>";

error_reporting(E_ALL);

$foo = NULL;
var_dump(is_null($inexistent), is_null($foo));


echo "<br><br>";
echo "..................isset...................";
echo "<br><br>";

$var = '';

if (isset($var)) {
    echo "This var is set so I will print.";
}

$a = "test";
$b = "anothertest";

var_dump(isset($a));
var_dump(isset($a, $b));

unset($a);

var_dump(isset($a));
var_dump(isset($a, $b));

$foo = NULL;
var_dump(isset($foo));


echo "<br><br>";
echo "..................isset...................";
echo "<br><br>";

$a = array ('a' => 'apple' , 'b' => 'banana' , 'c' => array('x' , 'y' , 'z'));
print_r($a);


echo "<br><br>";
echo "..................isset...................";
echo "<br><br>";


$a = 1000;
$b = .100;
$c = 1000.0;

echo gettype(a);
echo gettype(b);
echo gettype(c);







