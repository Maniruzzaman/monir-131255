<?php

class Loging
{
    public $usernamesignup = '';
    public $emailsignup = '';
    public $passwordsignup = '';
    public $passwordsignup_confirm = '';

public function setData($data = '')
{
    if (array_key_exists("usernamesignup", $data)) {
        $this->usernamesignup = $data['usernamesignup'];
    }
    if (array_key_exists("emailsignup", $data)) {
        $this->emailsignup = $data['emailsignup'];
    }
    if (array_key_exists("passwordsignup", $data)) {
        $this->passwordsignup =$data['passwordsignup'];
    }
    if (array_key_exists("passwordsignup_confirm", $data)) {
        $this->passwordsignup_confirm =$data['passwordsignup_confirm'];
    }

    return $data;
}
    public function store()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=theme', 'root', '');

            $query = "INSERT INTO `loging` (`usernamesignup`, `emailsignup`, `passwordsignup`, `passwordsignup_confirm`) VALUES (:a, :b, :c, :d)";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' => $this->usernamesignup,
                ':b' => $this->emailsignup,
                ':c' => $this->passwordsignup,
                ':d' => $this->passwordsignup_confirm
            ));
            if($stmt){
                session_start();
                $_SESSION['message'] = "Successfully Submitted";
                header('location:index.php#toregister');
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}