<?php
error_reporting(E_ALL);
error_reporting(E_ALL & ~ E_DEPRECATED);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if(PHP_SAPI == 'cli')
    die('This example should only be run from a Web Browser');

/** Include PHPExcel */

require_once ('../../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php');
include_once ("../../../../Src/Module/BITM/Exam/Students.php");

$obj = new Students();
$allData = $obj->index();

// create new PHPExcel
$objPHPExcel = new PHPExcel();
//Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

// Add some data

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'SL')
    ->setCellValue('B1', 'ID')
    ->setCellValue('C1', 'Name');
$counter = 2;
$serial = 0;
foreach ($allData as $data){
    $serial ++;
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$counter, $serial)
        ->setCellValue('B'.$counter, $data['id'])
        ->setCellValue('C'.$counter, $data['title']);
    $counter++;
}

// Rename worksheet

$objPHPExcel->getActiveSheet()->setTitle('Mobile_List');

// Set active sheet index tot he first sheet, so Excel opens tihs as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client's web browser (Excel5)

header('Content-Type: aplication/vnd.ms-excel');
header('Content-Disposition: attachment;filename="01simple.xls"');
header('Cache-Control: max-age=0');

// If you're serving to IE 9, then the following may be needed
header('Expires: Mon, 26 Jul 19997 05:00:00 GMT'); // Date in the past
header('Last-Modified: '.gmdate('d, d M Y H:i:s').' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;






