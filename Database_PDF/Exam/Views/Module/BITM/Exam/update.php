<?php
include_once("../../../../Src/Module/BITM/Exam/Students.php");

session_start();
$obj = new Students();
if(!empty($_POST['title'])){
    if(preg_match("/([a-z])/", $_POST['title'])){
        $obj->setData($_POST);
        $obj->update();
    }else{
        $_SESSION['message'] = "Invalid Input";
        header('location:index.php');
    }

}else{
    $_SESSION['message'] = "Input can't be empty";
    header('location:index.php');
}