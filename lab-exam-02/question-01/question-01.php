<?php
//Question on 01-A
echo "Answer to the question on 01-A" . "<br>";

$array1 = array("color" => "red", "black", "white", 2, 4);
$array2 = array("a", "b", "color" => "green", "shape" => "trapezoid", 4);
$result = array_merge($array1, $array2);
echo "<pre>";
print_r($result);



//Question on 01-B
echo "Answer to the question on 01-B" . "<br>";

$a=array("Hasan","Jakir", "Arif", "Siddik", "Khokon", "Sobuj", "Kabir" , "Arju", "Tohid", "Sadik");
$reverse=array_reverse($a);

echo "<pre>";
print_r($a);
print_r($reverse);



echo "<br>" . "<br>";
//Question on 01-C
echo "Answer to the question on 01-C" . "<br>";
$array = array("color", "red", "black", "white", 2, 4);

$result = array_unique($array);

print_r($result);






echo "<br>" . "<br>";
//Question on 01-D
echo "Answer to the question on 01-D" . "<br>";

$a = array(1,2,3,4,5,6,7,8,9,10);
$sum = 0;
foreach($a as $sing_value){
    $sum = $sum + $sing_value;
}

echo $sum;



echo "<br>" . "<br>";
//Question on 01-F
echo "Answer to the question on 01-F" . "<br>";

$a=array("red","green","blue","black", "white");
$b = array(count($a));
print_r($b);




echo "<br>" . "<br>";
//Question on 01-I
echo "Answer to the question on 01-I" . "<br>";
$a=array("red","green","blue","black", "white");
array_pop($a);
echo "<pre>";
print_r($a);



echo "<br>" . "<br>";
//Question on 01-J
echo "Answer to the question on 01-J" . "<br>";
$a=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"black","e"=>"white");
echo array_search("green",$a);




