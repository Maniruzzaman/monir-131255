<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop PHP
 * Date: 12/31/2016
 * Time: 9:18 AM
 */
/*
echo dirname("/etc/passwd") . PHP_EOL;
echo dirname("/etc/") . PHP_EOL;
echo dirname(".") . PHP_EOL;
echo dirname("C:\\") . PHP_EOL;
echo dirname("/usr/local/lib");
*/

function safe_dirname($path)
{
    $dirname = dirname($path);
    return $dirname == '/' ? '' : $dirname;
}