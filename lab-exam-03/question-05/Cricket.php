<?php

/**
 * Created by PhpStorm.
 * User: Web App Develop PHP
 * Date: 1/7/2017
 * Time: 11:36 AM
 */
class Cricket
{
    public function __construct()
    {
        echo "This output is Cricket construct Class." . "<br>";

    }
}

class Cricket_Movie extends Cricket
{
    public function __construct(){
        parent::__construct();
        echo "This output is SubClass";
    }
}

$obj_subclass = new Cricket_Movie();





