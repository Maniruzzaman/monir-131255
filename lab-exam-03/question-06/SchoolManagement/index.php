<?php
//include_once("Src/BITM/SEIP131255/CourseManagement/CourseManagement.php");
//include_once("Src/BITM/SEIP131255/ExamManagement/ExamManagement.php");
//include_once("Src/BITM/SEIP131255/StudentManagement/StudentManagement.php");
//include_once("Src/BITM/SEIP131255/TeacherManagement/TeacherManagement.php");

include_once("vendor/autoload.php");

use App\BITM\SEIP131255\CourseManagement\CourseManagement;
use App\BITM\SEIP131255\ExamManagement\ExamManagement;
use App\BITM\SEIP131255\StudentManagement\StudentManagement;
use App\BITM\SEIP131255\TeacherManagement\TeacherManagement;
$obj = new CourseManagement();
$obj_exam = new ExamManagement();
$obj_student = new StudentManagement();
$obj_teacher = new TeacherManagement();