<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit3292999bdc447aefc0594f2cc1f22933
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/Src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit3292999bdc447aefc0594f2cc1f22933::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit3292999bdc447aefc0594f2cc1f22933::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
