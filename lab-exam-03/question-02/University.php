<?php


class University
{
    public $student= "Akbor Hossain";
    public $subject= "Computer";

    public function getName(){
        echo "Student name is "
            .$this->student;
    }

    public function getSubject(){
        echo " and Subject is " . $this->subject;
    }
}

class University_Student extends University
{
    public function getDetails(){
        return $this->getName().$this->getSubject();
    }
}

$obj = new University();
//$obj->getName();
//$obj->getSubject();
$obj_subClass = new University_Student();
$obj_subClass->getDetails();








