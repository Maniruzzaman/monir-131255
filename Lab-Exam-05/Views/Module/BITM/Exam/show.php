<?php
include_once("../../../../Src/Module/BITM/Exam/Students.php");

$obj = new Students();
$obj->setData($_GET);
$value = $obj->show();

?>

<html>
<head>
    <title>Student Information List</title>
</head>
<body>
<a href="index.php">See List</a> |
<a href="create.php">Add New</a>
<fieldset>
    <legend>Student Information</legend>
    <form>
        <table border="1px">
            <tr>
                <td>Id</td>
                <td>Name</td>
                <td>Hobies</td>
                <td>Action</td>
            </tr>

                <tr>
                    <td><?php echo $value['id'] ?></td>
                    <td><?php echo $value['title'] ?></td>
                    <td><?php echo $value['hobies'] ?></td>
                    <td>
                        <a href="edit.php?id=<?php echo $value["id"]; ?>">Edit</a> |
                        <a href="delete.php?id=<?php echo $value["id"]; ?>">Delete</a>
                    </td>
                </tr>
        </table>
    </form>
</fieldset>
</body>
</html>

