<?php


class User
{
    public $name = '';
    public $id = '';

    public function setData($data = '')
    {
        if(array_key_exists('title', $data)){
            $this->name = $data['title'];
        }
        if(array_key_exists('id', $data)){
            $this->id = $data['id'];
        }
        return $this;
    }


    public function index()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=exam', 'root', '');
            $query = "SELECT * FROM `students`";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function store()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=exam', 'root', '');
            $query = "INSERT INTO `students` (`id`, `title`) VALUES (:a, :b)";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' => null,
                ':b' => $this->name
            ));
            if($stmt){
                session_start();
                $_SESSION['message'] = "Successfully Submitted";
                header('location:create.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }

    public function show()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=exam', 'root', '');
            $query = "SELECT * FROM `students` WHERE id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();

            return $data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function update()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=exam', 'root', '');
            $query = "UPDATE students SET title=:title WHERE id=:id";
    //        $query = "UPDATE students SET title=:title where id=:id";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':id' => $this->id,
                ':title' => $this->name
            ));
            if($stmt){
                session_start();
                $_SESSION['message'] = "Successfully Updated";
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function delete()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=exam', 'root', '');
            $query = "DELETE FROM `students` WHERE `students`.`id` = $this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute();

            if ($stmt){
                session_start();
                $_SESSION['message'] = "Seccessfully Delete";
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}