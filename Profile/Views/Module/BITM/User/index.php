<?php
include_once("../../../../Src/Module/BITM/User/User.php");

$obj = new User();
$allData = $obj->index();

session_start();

if(isset($_SESSION['message'])){
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}

?>
<html>
<body>
<a href="create.php">Add List</a>
<table border="1px">
    <tr>
        <td>Id</td>
        <td>Name</td>
        <td>Action</td>
    </tr>
    <?php
    $serial = 1;
    foreach ($allData as $key => $value){

    ?>
    <tr>
        <td><?php echo $serial++ ?> </td>
        <td><?php echo $value['title'] ?></td>
        <td>
            <a href="show.php?id=<?php echo $value['id'] ?>">Views | </a>
            <a href="edit.php?id=<?php echo $value['id'] ?>"> Edit | </a>
            <a href="delete.php?id=<?php echo $value['id'] ?>"> Delete</a>
        </td>
    </tr>
    <?php } ?>
</table>
</body>
</html>


