<?php
include_once("../../../../Src/Module/BITM/User/User.php");

session_start();
$obj = new User();


if(!empty($_POST['title'])){
    if(preg_match("/([a-z])/", $_POST['title'])){
        $obj->setData($_POST)->update();
    }else{
        $_SESSION['message'] = "Invalid Input";
        header('location:create.php');
    }

}else{
    $_SESSION['message'] = "Input can't be empty";
    header('location:create.php');
}
