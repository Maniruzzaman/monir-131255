<?php
include_once("../../../../../../vendor/autoload.php");

use App\Module_1\University\Subjects\Batch\Students\Students
    ;
$obj = new Students();

$allData = $obj->index();

?>


<html>
<head>
    <title>List of Students</title>
</head>
<body>

<table border="1px">
    <tr>
        <td>Serial</td>
        <td>Title</td>
        <td>Action</td>
    </tr>
    <?php
    $serial = 1;

    foreach($allData as $key=>$value){
        ?>
        <tr>
            <td><?php echo $serial++ ?></td>
            <td><?php echo $value['title'] ?></td>
            <td>
                <a href="show.php?id=<?php echo $value['id'] ?>">View Details</a>
            </td>
        </tr>
    <?php }?>
</table>
</body>
</html>
