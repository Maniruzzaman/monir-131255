<?php
namespace App\Module1\Bitm\SIEP\Students;

class Students
{
   public $name = '';
    public $id = '';
    public $searchItem = '';



    public function setData($data = '')
    {
        if(array_key_exists('title', $data)){
            $this->name = $data['title'];
        }
        if(array_key_exists('id', $data)){
            $this->id = $data['id'];
        }
        return $this;
    }

    public function index()
    {
        try{
            $pdo = new \PDO('mysql:host=localhost; dbname=myproject', 'root', '');
            $query = "SELECT * FROM `students`";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;

        }catch (\PDOException $e){
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function store()
    {
        try{
            $pdo = new \PDO('mysql:host=localhost; dbname=myproject', 'root', '');
            $query = "INSERT INTO `students` (`id`, `name`) VALUES (:a, :b)";
            $stmt = $pdo->prepare($query);
            $stmt->execute(
                array(
                    ':a' => null,
                    ':b' => $this->name
                )
            );
            if($stmt){
                header('location:create.php');
            }
        }catch (\PDOException $e){
            echo 'Error: ' . $e->getMessage();
        }

    }

    public function show()
    {
        try{
            $pdo = new \PDO('mysql:host=localhost; dbname=myproject', 'root', '');
            $query = "SELECT * FROM `students` WHERE id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            return $data;
        }
        catch (\PDOException $e){
            echo 'Error: ' . $e->getMessage();
        }

    }

    public function update()
    {
        try{
            $pdo = new \PDO('mysql:host=localhost; dbname=myproject', 'root', '');
            $query = "UPDATE `students` SET `name`=:name WHERE id=:id";
            $stmt = $pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id,
                    ':name' => $this->name
                )
            );
            if($stmt){
                header('location:index.php');
            }
        }catch (\PDOException $e){
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function delete()
    {
        try{
            $pdo = new \PDO('mysql:host=localhost; dbname=myproject', 'root', '');
            $query = "DELETE FROM students WHERE students. id=$this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
        if($stmt) {
            header('location:index.php');
        }
        }
        catch (\PDOException $e){
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function search($searchData='')
    {
        $this->searchItem = $searchData['search'];


        try{
            $pdo = new \PDO('mysql:host=localhost; dbname=myproject', 'root', '');
            $query = "SELECT * FROM `students` WHERE `name` = '$this->searchItem'";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            return $data;

        }catch (\PDOException $e){
            echo 'Error: ' . $e->getMessage();
        }

    }

}