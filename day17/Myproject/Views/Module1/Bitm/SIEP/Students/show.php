<?php
include_once("../../../../../vendor/autoload.php");
use  App\Module1\Bitm\SIEP\Students\Students;

$obj = new Students();
$value = $obj->setData($_GET)->show();

?>

<html>
<head>
    <title>List of Students</title>
</head>
<body>
<a href="index.php">Back to list</a>
<table border="1">
    <tr>
        <th>Serial</th>
        <th>Title</th>
        <th>Action</th>

    </tr>
    <tr>
        <td><?php echo $value['id'] ?></td>
        <td><?php echo $value['name'] ?></td>
        <td>
            <a href="edit.php?id=<?php echo $value['id'] ?>">Edit</a> |
            <a href="delete.php?id=<?php echo $value['id'] ?>">Delete</a>
        </td>
    </tr>

</table>
</body>
</html>

