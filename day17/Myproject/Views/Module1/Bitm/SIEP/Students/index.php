<?php
include_once("../../../../../vendor/autoload.php");

use  App\Module1\Bitm\SIEP\Students\Students;

$obj = new Students();

$allData = $obj->index();
?>

<form action="search.php" method="POST">
    <table>
        <tr>
            <td><input type="text" name="search" /></td>
            <td><input type="submit" value="Search" /></td>
        </tr>
    </table>

</form>

<table border="1">
    <a href="create.php">Add New</a>
    <thead>
    <tr>
        <th>Serial</th>
        <th>Title</th>
        <th>Action</th>

    </tr>
    <?php
    $a = 1;
    foreach ($allData as $key => $value) {
        ?>
    <tr>
        <td><?php echo $a++ ?></td>
        <td><?php echo $value['name'] ?></td>
        <td>
            <a href="show.php?id=<?php echo $value['id'] ?>">View Details</a> |
            <a href="edit.php?id=<?php echo $value['id'] ?>">Edit</a> |
            <a href="delete.php?id=<?php echo $value['id'] ?>">Delete</a>
        </td>
    </tr>

    <?php } ?>
    </thead>

</table>
