<?php
include_once("../../../../../vendor/autoload.php");
use  App\Module1\Bitm\SIEP\Students\Students;

$obj = new Students();
$value = $obj->setData($_GET)->show();


?>


<html>
<head>
    <title>Student information</title>
</head>
<fieldset>
    <legend>Update student information</legend>

    <form action="update.php" method="POST">
        <input type="text" name="title" value="<?php echo $value['name'] ?>"/>
        <input type="hidden" name="id" value="<?php echo $value['id'] ?>" />
        <input type="submit" value="Update"/>
    </form>
</fieldset>
</html>
