<?php
$a = 2;
$b = 3;

if ($a == $b){
    echo "Equal";
}else {
    echo "Not Equal";
}

echo "<br>";

if ($a === $b){
    echo "Equal";
}else {
    echo "Not Equal";
}

echo "<br>";

if ($a != $b){
    echo "Equal";
}else {
    echo "Not Equal";
}

echo "<br>";

if ($a <> $b){
    echo "Equal";
}else {
    echo "Not Equal";
}

echo "<br>";

if ($a !== $b){
    echo "Equal";
}else {
    echo "Not Equal";
}

echo "<br>";

if ($a > $b){
    echo "Equal";
}else {
    echo "Not Equal";
}

echo "<br>";

if ($a < $b){
    echo "Equal";
}else {
    echo "Not Equal";
}

echo "<br>";

if ($a >= $b){
    echo "Equal";
}else {
    echo "Not Equal";
}

echo "<br>";

if ($a <= $b){
    echo "Equal";
}else {
    echo "Not Equal";
}

echo "<br>";
