<?php
namespace App\SEIP\Students;
use PDO;
class Students{
	
	public $u_id='';
	public $title='';
	public $password='';
	public $sex='';
	public $subject='';
	public $groups='';

	public function setData($data=''){
		if(array_key_exists('id',$data)){
			$this->u_id = $data['id'];
		}
		if(array_key_exists('title',$data)){
			$this->title = $data['title'];
		}
		if(array_key_exists('password',$data)){
			$this->password = $data['password'];
		}
		if(array_key_exists('sex',$data)){
			$this->sex = $data['sex'];
		}
		if(array_key_exists('subject', $data)){
			$this->subject = $data['subject'];
		}
		if(array_key_exists('groups', $data)){
			$this->groups = $data['groups'];
		}		
		return $this;
	}
	public function store(){
		$password_md5 = md5($this->password);
		$allsubject = serialize($this->subject);
		try {
		$pdo = new PDO('mysql:host=localhost;dbname=php-38', 'root', '');
		$query = "INSERT INTO `students`(`id`,`unique_id`,`title`,`password`,`sex`,`subject`,`groups`) VALUES(:a,:b,:c,:d,:e,:f,:g)";
		$stmt = $pdo->prepare($query);
		  $stmt->execute(array(
		  	':a' =>null,
		    ':b' => uniqid(),
		    ':c' => $this->title,
		    ':d' => $password_md5,
		    ':e' => $this->sex,
		    ':f' => $allsubject,
		    ':g' => $this->groups,
		  ));
		  if ($stmt) {
		  	session_start();
		  	$_SESSION['message']="Data inserted successfully.";
		  	header('location:create.php');
		  }
		} catch(PDOException $e) {
		  echo 'Error: ' . $e->getMessage();
		}
	}
	public function index($noOfItems='', $offset){
		try {
			$pdo = new PDO('mysql:host=localhost;dbname=php-38', 'root', '');
			$query = "SELECT * FROM students WHERE deleted_at='0000-00-00 00:00:00' ORDER BY id DESC LIMIT $noOfItems OFFSET $offset ";
			$stmt = $pdo->prepare($query);
			$stmt->execute();
			$data = $stmt->fetchall();
			return $data;
		} catch(PDOException $e) {
		  echo 'Error: ' . $e->getMessage();
		}
	}
	public function noOfRows(){
		try {
			$pdo = new PDO('mysql:host=localhost;dbname=php-38', 'root', '');
			$query = "SELECT * FROM students";
			$stmt = $pdo->prepare($query);
			$stmt->execute();
			$data = count($stmt->fetchall());
			return $data;
		} catch(PDOException $e) {
		  echo 'Error: ' . $e->getMessage();
		}
	}
	public function show(){
		try {
			$pdo = new PDO('mysql:host=localhost;dbname=php-38', 'root', '');
			$query = "SELECT * FROM students WHERE unique_id = '$this->u_id'";
			$stmt = $pdo->prepare($query);
			$stmt->execute();
			$data = $stmt->fetch();
			if(empty($data)){
				session_start();
				$_SESSION['message']="Oop invalid input.";
				header("location:index.php");
			}else{
				return $data;
			}
		} catch(PDOException $e) {
		  echo 'Error: ' . $e->getMessage();
		}
	}
	public function update(){
		try {
			$password_md5 = md5($this->password);
			$allsubject = serialize($this->subject);
			$pdo = new PDO('mysql:host=localhost;dbname=php-38', 'root', '');
			$query = "UPDATE students SET `title`=:title,`password`=:password,`sex`=:sex,`subject`=:subject,`groups`=:groups WHERE unique_id = '$this->u_id'";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array(
				':title'=>$this->title,
				':password'=>$password_md5,
				':sex'=>$this->sex,
				':subject'=>$allsubject,
				':groups'=>$this->groups,
			));
			if($stmt){
				session_start();
	            $_SESSION['message']= "Data updated successfully.";
	            header('location:index.php');
	        }
		} catch(PDOException $e) {
		  echo 'Error: ' . $e->getMessage();
		}
	}
	public function trash(){
		try {
			$password_md5 = md5($this->password);
			$allsubject = serialize($this->subject);
			$pdo = new PDO('mysql:host=localhost;dbname=php-38', 'root', '');
			$query = "UPDATE students SET `deleted_at`=:softdelet WHERE unique_id = '$this->u_id'";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array(
				':softdelet'=>date('Y-m-d h:m:s'),
			));
			if($stmt){
				session_start();
	            $_SESSION['message']= "Data deleted successfully But store Recycle bin.";
	            header('location:index.php');
	        }
		} catch(PDOException $e) {
		  echo 'Error: ' . $e->getMessage();
		}
	}
	public function trashlist($noOfItems='', $offset){
		try {
			$pdo = new PDO('mysql:host=localhost;dbname=php-38', 'root', '');
			$query = "SELECT * FROM students WHERE deleted_at!='0000-00-00 00:00:00' ORDER BY id DESC LIMIT $noOfItems OFFSET $offset ";
			$stmt = $pdo->prepare($query);
			$stmt->execute();
			$data = $stmt->fetchall();
			return $data;
		} catch(PDOException $e) {
		  echo 'Error: ' . $e->getMessage();
		}
	}
	public function restore(){
		try {
			$password_md5 = md5($this->password);
			$allsubject = serialize($this->subject);
			$pdo = new PDO('mysql:host=localhost;dbname=php-38', 'root', '');
			$query = "UPDATE students SET `deleted_at`=:softdelet WHERE unique_id = '$this->u_id'";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array(
				':softdelet'=>'0000-00-00 00:00:00',
			));
			if($stmt){
				session_start();
	            $_SESSION['message']= "Data Restore successfully.";
	            header('location:index.php');
	        }
		} catch(PDOException $e) {
		  echo 'Error: ' . $e->getMessage();
		}
	}
	public function delete(){
		try {
			$pdo = new PDO('mysql:host=localhost;dbname=php-38', 'root', '');
			$query = "DELETE FROM students WHERE unique_id = '$this->u_id'";
			$stmt = $pdo->prepare($query);
			$stmt->execute();
			if ($stmt=='') {
				session_start();
				$_SESSION['message']="Oop invalid input.";
				header('location:trashlist.php');
				
			}else{
				session_start();
				$_SESSION['message']="Data deleted permanently.";
				header('location:trashlist.php');
			}
		} catch(PDOException $e) {
		  echo 'Error: ' . $e->getMessage();
		}
	}
}


