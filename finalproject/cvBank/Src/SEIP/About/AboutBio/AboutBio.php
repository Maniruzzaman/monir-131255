<?php
namespace App\SEIP\About\AboutBio;
use PDO;
Class AboutBio{
	private $user_id='';
	private $bioTitle='';
	private $bioPhone='';
	private $bioDescription='';
	public function setData($data=''){
		if (array_key_exists("id", $data)) {
			$this->user_id = $data['id'];
		}
		if(array_key_exists('bio_title', $data)){
			$this->bioTitle = $data['bio_title'];
		}
		if(array_key_exists('bio_phone', $data)){
			$this->bioPhone = $data['bio_phone'];
		}
		if(array_key_exists('bio_description', $data)){
			$this->bioDescription = $data['bio_description'];
		}
		return $this;
	}
	public function store(){
		try {
		$pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
		$query = "INSERT INTO `abouts`(`id`,`user_id`,`title`,`phone`,`bio`) VALUES(:a,:b,:c,:d,:e)";
		$stmt = $pdo->prepare($query);
		  $stmt->execute(array(
		  	':a' =>null,
		    ':b' => uniqid(),
		    ':c' => $this->bioTitle,
		    ':d' => $this->bioPhone,
		    ':e' => $this->bioDescription,
		  ));
		  if ($stmt) {
		  	session_start();
		  	$_SESSION['message']="Data inserted successfully.";
		  	header('location:create.php');
		  }
		} catch(PDOException $e) {
		  echo 'Error: ' . $e->getMessage();
			
		}
	}
	public function index(){
		try {
		$pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
		$query = "SELECT * FROM `abouts` ORDER BY id DESC";
		$stmt = $pdo->prepare($query);
		$stmt->execute();
		$data = $stmt->fetchall();
		return $data;
		} catch(PDOException $e) {
		  echo 'Error: ' . $e->getMessage();			
		}
	}
	public function show(){
		try {

			$pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
			$query = "SELECT * FROM abouts WHERE user_id = '$this->user_id'";
			$stmt = $pdo->prepare($query);
			$stmt->execute();
			$data = $stmt->fetch();
			if(empty($data)){
				session_start();
				$_SESSION['message']="Oop invalid input.";
				header("location:index.php");
			}else{
				return $data;
			}
		} catch(PDOException $e) {
		  echo 'Error: ' . $e->getMessage();
		}
	}
	public function update(){
		try {

			$pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
			$query = "UPDATE abouts SET `title`=:title,`phone`=:phone,`bio`=:bio WHERE user_id = '$this->user_id'";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array(
				':title'=>$this->bioTitle,
				':phone'=>$this->bioPhone,
				':bio'=>$this->bioDescription,
			));
			if($stmt){
				session_start();
	            $_SESSION['message']= "Data updated successfully.";
	            header('location:index.php');
	        }
		} catch(PDOException $e) {
		  echo 'Error: ' . $e->getMessage();
		}
	}
	public function delete(){
		try {
			$pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
			$query = "DELETE FROM abouts WHERE user_id = '$this->user_id'";
			$stmt = $pdo->prepare($query);
			$stmt->execute();
			if ($stmt=='') {
				session_start();
				$_SESSION['message']="Oop invalid input.";
				header('location:index.php');
				
			}else{
				session_start();
				$_SESSION['message']="Data deleted permanently.";
				header('location:index.php');
			}
		} catch(PDOException $e) {
		  echo 'Error: ' . $e->getMessage();
		}
	}

}