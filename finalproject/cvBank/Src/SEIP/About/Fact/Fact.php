<?php
namespace App\SEIP\About\Fact;
use PDO;
class Fact
{
    public $name = '';
    public $fact = '';
    public $image = '';
    public $user = '';

    public function setData($data = '')
    {
        $this->name = $data['title'];

        return $this;
    }

    public function getData()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
           $query = "INSERT INTO `facts` (`id`, `user_id`, `title`, `no_of_items`, `img`, `created_at`, `updated_at`, `deleted_at`) VALUES (:a, :b, :c, :d, :e)";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' => null,
                ':b' => $this->user,
                ':c' => $this->name,
                ':d' => $this->fact,
                ':e' => $this->image
            ));

            # Affected Rows?
            echo $stmt->rowCount(); // 1
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
    }
}
}