<?php
    include_once "../../../vendor/mpdf/mpdf/mpdf.php";
    include_once "../../../vendor/autoload.php";
    use App\SEIP\Students\Students;
    $obj = new Students();
    $value = $obj->setData($_GET)->show();
    $arr = unserialize($value['subject']);
    $allsub = implode(",",$arr);

    $trs1 ="<td>".$value['title']."</td>";
    $trs2 ="<td>".$allsub."</td>";
    $trs3 ="<td>".$value['groups']."</td>";
    $trs4 ="<td>".$value['sex']."</td>";


$html = <<<EOD
<!doctype html>
    <head>
        <title>List of Books</title>
    </head>
    <body>
      <table border="1" align="center">
        <tr><td> Name  </td>$trs1;</tr>
        <tr><td> Subject  </td>$trs2;</tr>
        <tr><td> Group  </td>$trs3;</tr>
        <tr><td> Gender  </td>$trs4;</tr>
    </table>  
    </body>
EOD;

    $mpdf=new mPDF();
    $mpdf->WriteHTML($html);
    $mpdf->Output();
    exit();
?>