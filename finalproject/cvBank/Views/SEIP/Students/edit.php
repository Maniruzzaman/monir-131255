<?php

	include_once "../../../vendor/autoload.php";
	use App\SEIP\Students\Students;
	$obj = new Students();
	$value = $obj->setData($_GET)->show();
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Student Information</title>
</head>
<body>
	<a href="index.php">View List</a>
	<div style="width: 500px;margin:0 auto;">
		<fieldset>
			<legend>Add student information</legend>
			<form action="update.php" method="POST">
			<input type="hidden" name="id" value="<?php echo $value['unique_id']; ?>">
				<table>
					<tr>
						<td>Name : </td>
						<td><input type="text" name="title" value="<?php echo $value['title']; ?>"></td>
					</tr>
					<tr>
						<td>Password : </td>
						<td><input type="password" name="password" value="<?php echo $value['password']; ?>"></td>
					</tr>
					<tr>
						<td>Gender : </td>
						<td>
							<?php
							$checkedMale='';
							$checkedFemale='';
								$radio_data  = $value['sex'];
								if ($radio_data == "Male") {
									$checkedMale = "checked";
								}
								if ($radio_data == "Female") {
									$checkedFemale = "checked";
								}
								
							?>
							<input type="radio" name="sex" value="Male" <?php echo $checkedMale; ?> />Male
							<input type="radio" name="sex" value="Female" <?php echo $checkedFemale; ?> />Female
						</td>
					</tr>
					<tr>
						<td>Subject : </td>
						<td>
						<?php
							$che_data  = unserialize($value['subject']);
						?>
<input type="checkbox" name="subject[]" value="Bangla" 
<?php if(in_array('Bangla', $che_data)): echo ' checked="checked"'; endif; ?> />Bangla
<input type="checkbox" name="subject[]" value="English" <?php if(in_array('English', $che_data)): echo ' checked="checked"'; endif; ?> />English
<input type="checkbox" name="subject[]" value="Physic" <?php if(in_array('Physic', $che_data)): echo ' checked="checked"'; endif; ?> />Physic
						</td>
					</tr>
					<tr>
						<td>Group : </td>
						<td>
							<select name="groups" id="">
								<option>Select group</option>
								<?php
									$optionSci  ='';
									$optionBuss ='';
									$optionArt  ='';
										$option_data  = $value['groups'];
										if ($option_data == "science") {
											$optionSci = "selected";
										}
										if ($option_data == "bussness") {
											$optionBuss = "selected";
										}
										if ($option_data == "arts") {
											$optionArt = "selected";
										}
										
									?>
								<option value="science" <?php echo $optionSci; ?> >Science</option>
								<option value="bussness" <?php echo $optionBuss; ?> >Bussness Studias</option>
								<option value="arts" <?php echo $optionArt; ?> >Arts</option>
							</select>
						</td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" value="update" name="form1"></td>
					</tr>
				</table>
			</form>
			
		</fieldset>	
	</div>
</body>
</html>