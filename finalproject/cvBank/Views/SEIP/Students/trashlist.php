<?php

	include_once "../../../vendor/autoload.php";
	use App\SEIP\Students\Students;
	$obj = new Students();
	$noOfItemsPorPage = 3;
	$totalRows = $obj->noOfRows();
	$totalPage = ceil($totalRows / $noOfItemsPorPage);

	if(!empty($_GET['page'])){
		$currentPage = $_GET['page']- 1;
	}else{
		$currentPage=0;
	}
	$offset = $noOfItemsPorPage * $currentPage ;
	
	$alldata = $obj->trashlist($noOfItemsPorPage, $offset);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>View all Data</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script>
		function confirmDelete(){
			return confirm("Do you sute want to delete this data?");
		}
	</script>
	<style type="text/css">
		/*a{text-decoration: none;}*/
	</style>
</head>
<body>
<?php
		session_start();
		if(isset($_SESSION['message'])){
		echo $_SESSION['message'];
		unset($_SESSION['message']);
	}
?>
	<table border="1" align="center">
	<a href="create.php">Create user</a> || <a href="trashlist.php">Recycle Bin</a> || <a href="index.php">See Original List</a>
		<thead>
			<tr>
				<th width="5%">Serial  </th>
				<th width="5%">ID  </th>
				<th width="45%">Name  </th>
				<th width="45%">Action  </th>
			</tr>
		</thead>
		<tbody>
		<?php
			$i=$offset;
			foreach ($alldata as $key => $value) {
					$i++;
				?>
					<tr>
						<td align="center"><?php echo $i; ?></td>
						<td align="left"><?php echo $value['id']; ?></td>
						<td align="left"><?php echo $value['title']; ?></td>
						<td align="center">
							<a href="show.php?id=<?php echo $value['unique_id']; ?>">View</a> | 
							<a href="edit.php?id=<?php echo $value['unique_id']; ?>">Edit</a> | 
							<a href="restore.php?id=<?php echo $value['unique_id']; ?>">Restore</a> | 
							<a onclick='return confirmDelete();' href="delete.php?id=<?php echo $value['unique_id']; ?>" >Delete</a>
						</td>
					</tr>
				<?php
			}
		?>
		</tbody>
	</table>
	<?php
		for ($i=1; $i<=$totalPage; $i++) { ?>
			<a href="index.php?page=<?php echo $i ; ?>"><?php echo $i; ?></a>
	<?php	}

	?>
</body>
</html>