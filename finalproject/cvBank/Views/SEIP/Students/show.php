<?php

	include_once "../../../vendor/autoload.php";
	use App\SEIP\Students\Students;
	$obj = new Students();
	$value = $obj->setData($_GET)->show();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Unique Identification user</title>
</head>
<body>
	<table border="1" align="center">
		<tr><td> Name  </td><td><?php echo $value['title']; ?></td></tr>
		<tr><td> Subject  </td><td>
			<?php  
				$arr = unserialize($value['subject']);
				echo implode(",",$arr);
			 ?>			
		</td></tr>
		<tr><td> Group  </td><td><?php echo $value['groups']; ?></td></tr>
		<tr><td> Gender  </td><td><?php echo $value['sex']; ?></td></tr>
		<tr>
		<a href="pdf.php?id=<?php echo $value['unique_id'];?>">Save as pdf </a> || 
		<a href="xl.php?id=<?php echo $value['unique_id'];?>">Save as XL </a> || 
		<a href="index.php">Back to List</a></tr>
	</table>

</body>

</html>