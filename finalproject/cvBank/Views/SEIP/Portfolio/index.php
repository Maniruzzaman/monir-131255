<?php
session_start();
if (empty($_SESSION['user_info'])) {
    $_SESSION['fail'] = "Sory! Your are not authorized to access this page";
    header('location:../index.php');
}
/*echo "<pre>";
print_r($_SESSION['user_info']);
die();*/
?>
<?php
include "../../../vendor/autoload.php";
use App\SEIP\Porfolio\Porfolio;
$obj = new Porfolio();
$allData = $obj->index();
/*echo "<pre>";
print_r($allData);*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>CvBank allkinds of cv here</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="../../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="../../../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../../../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="../../../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="../../../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="../../../assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="../../../assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="../../../assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="../../../assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="../../../assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script type="text/javascript" src="../../../assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script type="text/javascript" src="../../../assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="../../../assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="../../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="../../../assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="../../../assets/js/plugins/pickers/daterangepicker.js"></script>

		<!-- Theme JS files -->
	<script type="text/javascript" src="../../../ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="../../../assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="../../../assets/js/plugins/forms/styling/uniform.min.js"></script>
	
	<script type="text/javascript" src="../../../assets/js/core/app.js"></script>	
	<script type="text/javascript" src="../../../assets/js/pages/editor_ckeditor.js"></script>
	<script type="text/javascript" src="../../../assets/js/pages/form_layouts.js"></script>
	<script type="text/javascript" src="../../../assets/js/pages/dashboard.js"></script>
	<!-- /theme JS files -->

</head>

<body>
	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="index.html"><img src="../../../assets/images/logo_light.png" alt=""></a>
			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-git-compare"></i>
						<span class="visible-xs-inline-block position-right">Git updates</span>
						<span class="badge bg-warning-400">9</span>
					</a>					
					<div class="dropdown-menu dropdown-content">
						<div class="dropdown-content-heading">
							Git updates
							<ul class="icons-list">
								<li><a href="#"><i class="icon-sync"></i></a></li>
							</ul>
						</div>
						<ul class="media-list dropdown-content-body width-350">
							<li class="media">
								<div class="media-left">
									<a href="#" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-pull-request"></i></a>
								</div>

								<div class="media-body">
									Drop the IE <a href="#">specific hacks</a> for temporal inputs
									<div class="media-annotation">4 minutes ago</div>
								</div>
							</li>
						</ul>
						<div class="dropdown-content-footer">
							<a href="#" data-popup="tooltip" title="All activity"><i class="icon-menu display-block"></i></a>
						</div>
					</div>
				</li>
			</ul>
			<p class="navbar-text"><span class="label bg-success-400">Online</span></p>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown language-switch">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="../../../assets/images/flags/gb.png" class="position-left" alt="">English <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a class="deutsch"><img src="../../../assets/images/flags/de.png" alt=""> Deutsch</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-bubbles4"></i>
						<span class="visible-xs-inline-block position-right">Messages</span>
						<span class="badge bg-warning-400">2</span>
					</a>
					
					<div class="dropdown-menu dropdown-content width-350">
						<div class="dropdown-content-heading">
							Messages
							<ul class="icons-list">
								<li><a href="#"><i class="icon-compose"></i></a></li>
							</ul>
						</div>

						<ul class="media-list dropdown-content-body">
							<li class="media">
								<div class="media-left"><img src="../../../assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></div>
								<div class="media-body">
									<a href="#" class="media-heading">
										<span class="text-semibold">Bidhan</span>
										<span class="media-annotation pull-right">Mon</span>
									</a>									
									<span class="text-muted">Hi, bidhan sutradhar ....</span>
								</div>
							</li>
						</ul>
						<div class="dropdown-content-footer">
							<a href="#" data-popup="tooltip" title="All messages"><i class="icon-menu display-block"></i></a>
						</div>
					</div>
				</li>
				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="../../../assets/images/placeholder.jpg" alt="">
						<span><?php echo $_SESSION['user_info']['username'] ?></span>
						<i class="caret"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
						<li><a href="#"><i class="icon-coins"></i> My balance</a></li>
						<li><a href="#"><span class="badge bg-teal-400 pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
						<li><a href="logout.php"><i class="icon-switch2"></i> Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->
	
	<!-- Page container -->
	<div class="page-container">
		<!-- Page content -->
		<div class="page-content">
			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">
					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="../../../assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold">CoderGand</span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Dhaka,Bitm
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->
					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">
								<li class="active">
									<a href="../Admin/index.php"><i class="icon-home4"></i><span>Dashboard</span></a>
								</li>					
								<li>
									<a href="#"><i class="icon-insert-template"></i> <span>About</span></a>
									<ul>
										<li><a href="#">About Bio</a>
											<ul>
												<li><a href="../About/AboutBio/create.php">Add Bio</a></li>
												<li><a href="#">Edit Bio</a></li>
												<li><a href="#">View Bio</a></li>
											</ul>
										</li>
										<li><a href="#">About Hobby</a>
											<ul>
												<li><a href="../About/Hobbie/create.php">Add hobby</a></li>
												<li><a href="#">Edit hobby</a></li>
												<li><a href="#">About hobby</a></li>
											</ul>
										</li>
										<li><a href="#">About fact</a>
											<ul>
												<li><a href="../About/Fact/create.php">Add fact</a></li>
												<li><a href="#">Edit fact</a></li>
												<li><a href="#">View fact</a></li>
											</ul>
										</li>
									</ul>
								</li>				
								<li>
									<a href="#"><i class="icon-insert-template"></i> <span>Resume</span></a>
									<ul>
										<li><a href="#">Add Education</a>
											<ul>
												<li><a href="../Resume/Education/create.php">Add education</a></li>
												<li><a href="#">Edit education</a></li>
												<li><a href="#">View education</a></li>
											</ul>
										</li>
										<li><a href="#">Add Experience</a>
											<ul>
												<li><a href="../Resume/Experience/create.php">Add experience</a></li>
												<li><a href="#">Edit experience</a></li>
												<li><a href="#">View experience</a></li>
											</ul>
										</li>
										<li><a href="#">Add Awards</a>
											<ul>
												<li><a href="../Resume/Award/create.php">Add awards</a></li>
												<li><a href="#">Edit awards</a></li>
												<li><a href="#">View awards</a></li>
											</ul>
										</li>
									</ul>
								</li>			
								<li>
									<a href="#"><i class="icon-insert-template"></i> <span>Publications</span></a>
									<ul>
										<li><a href="../Post/create.php">Add post</a></li>
										<li><a href="#">Edit post</a></li>
										<li><a href="#">View post</a></li>
									</ul>
								</li>			
								<li>
									<a href="#"><i class="icon-insert-template"></i> <span>Services</span></a>
									<ul>
										<li><a href="../Service/create.php">Add service</a></li>
										<li><a href="#">Edit service</a></li>
										<li><a href="#">View service</a></li>
									</ul>
								</li>		
								<li>
									<a href="#"><i class="icon-insert-template"></i> <span>Skills</span></a>
									<ul>
										<li><a href="../Skill/create.php">Add skills</a></li>
										<li><a href="#">Edit skills</a></li>
										<li><a href="#">View skills</a></li>
									</ul>
								</li>	
								<li>
									<a href="#"><i class="icon-insert-template"></i> <span>Portfolios</span></a>
									<ul>
										<li><a href="../Portfolio/create.php">Add portfolio</a></li>
										<li><a href="#">Edit portfolio</a></li>
										<li><a href="#">View portfolio</a></li>
									</ul>
								</li>
								<li>
									<a href="#"><i class="icon-insert-template"></i> <span>Contact</span></a>
									<ul>
										<li><a href="../contact/create.php">Add contact</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->
			
			<!-- Main content -->
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Dashboard</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Contact</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<!-- Content area -->
				<div class="content">


					
					<!-- Main charts -->
					<div class="row">
						<div class="col-lg-12">
						<?php
                            if(isset($_SESSION['message'])){
                                echo "<h1 style='color:red;font-weight:bold;'>".$_SESSION['message']."</h1>";
                                unset($_SESSION['message']);
	                            }
	                        ?>
							<!-- Basic layout-->
							<table>
								<?php
									foreach ($allData as $value) {
										?>
										<tr><td><b>Portifolio title</b>  </td> <td><?php echo $value['title']; ?></td></tr>
										<tr><td><b>Portifolio Details </b>  </td> <td> <?php echo $value['description']; ?></td></tr>
										<tr><td><b>Portifolio Image</b> </td></tr>
										<tr><td><img src='<?php echo "../../../images/portfolio/".$value['img'];?> ' width="100"  height="100" alt=""></td></tr>
										<tr><td>
											<a href="edit.php?id=<?php echo $value['id']; ?>&user_id=<?php echo $value['user_id']; ?>">Edit</a> || 
											<a href="delete.php?id=<?php echo $value['id']; ?>&user_id=<?php echo $value['user_id']; ?>">Delete</a>
										</td></tr> 								
										<?php
									}			
								?>
							</table>
							<!-- /basic layout -->
						</div>
					</div>
					<!-- /main charts -->
					
					<!-- Footer -->
					<div class="footer text-muted">&copy; 2017. <a href="#">CvBank</a> by <a href="#" target="_blank">CodeGand</a>
					</div>
					<!-- /footer -->
					
				</div>
				<!-- /content area -->
			</div>
			<!-- /main content -->
		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->
</body>
</html>
