<?php
include_once "../../../vendor/autoload.php";
use App\SEIP\User\User;
$obj = new User();
$data = $obj->getUserTokenInfo($_GET['token']);
if(!empty($data)){
    if($data['is_active'] == 1){
        $_SESSION['fail']="Already varify now you login.";
        header('location:../../../Views/SEIP/User/index.php');
    }else{
        $obj->getVerify($data['id']);
    }
}else{
    $_SESSION['fail'] = "Invalid Request";
    header('location:../../../Views/SEIP/User/index.php');
}